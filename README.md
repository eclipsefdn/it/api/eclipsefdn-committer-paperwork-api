# eclipsefdn-committer-paperwork-api

Eclipse Foundation API for interacting with project committer paperwork. Facilitates the creation, provisioning, updating, and retirement workflows, as well as reading paperwork given some parameters. This API is currently in preview and is not available in production environments. Rough estimates for release is mid to late 2023 for production usage of this API.

[[_TOC_]]

## Application details

|Service name|Internal port|Host port|
|-|-|-|
|Application|8090|10119|
|FoundationDB API|8095|10120|
|MariaDB|3306|10121|

## Getting started

### Requirements

* Docker
* mvn
* make
* yarn
* Java 11 >

### Setup

Additional setup instructions will be added as the project grows and evolves.

#### Initial setup

This setup assumes that the user has Eclipse Foundation employee access as this is intended to be an internal facing API without usage outside of the Eclipse Foundation. The access requirements are access to the Webdev Bitwarden collection and to the internal servers where staging assets are stored, which are standard for developers in the IT team.

1. Run `make pre-install` to fetch required SQL files to populate database with required tables for operation.
2. Retrieve secrets the following secrets, and put them in the corresponding files. Secret `committer-paperwork-application-dev` should be set into `./config/application/secret.properties` and `committer-paperwork-fdb-dev` into `./config/foundation/secret.properties`.
3. Run `make compile-start` to compile and start associated services.

#### Build and start server

```bash
make compile-start
```

#### Live coding dev mode

```bash
make dev-start
```

#### Generate spec

```bash
make compile-test-resources
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [eclipsefdn-committer-paperwork-api](https://gitlab.eclipse.org/eclipsefdn/it/api/eclipsefdn-committer-paperwork-api) repository
2. Clone repository `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/eclipsefdn-committer-paperwork-api.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
