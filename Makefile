SHELL = /bin/bash
pre-install:;
	mkdir -p ./config/mariadb/initdb.d
	[ -f ./config/mariadb/initdb.d/eclipsefoundation.sql ] && echo "EF DB dump already exists, skipping fetch" || (scp api-vm1:~/webdev/sql/eclipsefoundation.sql.gz ./config/mariadb/initdb.d/ && gzip -d ./config/mariadb/initdb.d/eclipsefoundation.sql.gz )
	[ -f ./config/mariadb/initdb.d/eclipse-no-data.sql ] && echo "Eclipse DB dump already exists, skipping fetch" || (scp api-vm1:~/webdev/sql/eclipse-no-data.sql.gz ./config/mariadb/initdb.d/ && gzip -d ./config/mariadb/initdb.d/eclipse-no-data.sql.gz && sed -i '1s/^/USE eclipse;\n/' ./config/mariadb/initdb.d/eclipse-no-data.sql)
	[ -f ./config/mariadb/initdb.d/api.eclipse.org.sql ] && echo "Eclipse API DB dump already exists, skipping fetch" || (scp api-vm1:~/webdev/sql/api.eclipse.org.sql.gz ./config/mariadb/initdb.d/ && gzip -d ./config/mariadb/initdb.d/api.eclipse.org.sql.gz && sed -i '1s/^/USE eclipse_api;\n/' ./config/mariadb/initdb.d/api.eclipse.org.sql)

dev-start:;
	mvn compile -e quarkus:dev -Dconfig.secret.path=$$PWD/config/application/secret.properties

clean:;
	mvn clean

install-yarn:;
	yarn install --frozen-lockfile --audit

compile-test-resources: install-yarn;
	yarn run generate-json-schema

compile-java: compile-test-resources;
	mvn compile package -Dquarkus.test.profile=dev

compile-java-quick: compile-test-resources;
	mvn compile package -Dmaven.test.skip=true

compile: clean compile-java;

compile-quick: clean compile-java-quick;

compile-start: compile-quick;
	docker compose down
	docker compose build
	docker compose up -d

compile-startup: compile-quick;
	docker compose down
	docker compose build
	docker compose up