--
-- Creates DBs needed for API. 01 file name used to make script run first.
--
CREATE DATABASE eclipse_api;
CREATE DATABASE eclipse;
CREATE DATABASE eclipsefoundation;