-- eclipse.account_requests definition

CREATE TABLE `account_requests` (
  `email` varchar(100) NOT NULL,
  `new_email` varchar(100) DEFAULT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `req_when` datetime DEFAULT NULL,
  `token` varchar(64) DEFAULT NULL
);