-- eclipse_api.committer_paperwork definition

CREATE TABLE `committer_paperwork` (
  `id` int NOT NULL AUTO_INCREMENT,
  `forge` varchar(25) NOT NULL,
  `project_id` varchar(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `election_nid` int DEFAULT NULL,
  `election_status` int NOT NULL DEFAULT 9999,
  `committer_paperwork_nid` int DEFAULT NULL,
  `committer_paperwork_status` int NOT NULL DEFAULT 9999,
  `etag` varchar(255) NOT NULL,
  `created` int NOT NULL DEFAULT 0,
  `changed` int NOT NULL DEFAULT 0,
  `spec_project_working_group_tid` int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);

INSERT INTO committer_paperwork(id, forge, project_id, uid, election_nid, election_status, committer_paperwork_nid, committer_paperwork_status, etag, created, changed) 
  VALUES (
    123,
    'eclipse',
    'sample.project',
    1,
    0,
    9999,
    0,
    9999,
    '',
    1676052492,
    1676062492
);
INSERT INTO committer_paperwork(id, forge, project_id, uid, election_nid, election_status, committer_paperwork_nid, committer_paperwork_status, etag, created, changed) 
  VALUES (
    100123,
    'eclipse',
    'sample.project',
    1,
    0,
    9999,
    0,
    9999,
    '',
    1676052492,
    1676062492
);
INSERT INTO committer_paperwork(id, forge, project_id, uid, election_nid, election_status, committer_paperwork_nid, committer_paperwork_status, etag, created, changed) 
  VALUES (
    456,
    'eclipse',
    'sample.project',
    0,
    0,
    9999,
    0,
    9999,
    '',
    1676052492,
    1676062492
);
INSERT INTO committer_paperwork(id, forge, project_id, uid, election_nid, election_status, committer_paperwork_nid, committer_paperwork_status, etag, created, changed) 
  VALUES (
    789,
    'eclipse',
    'tech.nology',
    0,
    0,
    9999,
    0,
    9999,
    '',
    1676052492,
    1676062492
);