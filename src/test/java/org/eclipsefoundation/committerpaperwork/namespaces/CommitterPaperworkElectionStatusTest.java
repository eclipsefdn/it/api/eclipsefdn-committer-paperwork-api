/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.namespaces;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Tests the custom methods set in the enum for paperwork status.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class CommitterPaperworkElectionStatusTest {

    /**
     * Tests each of the status enums to ensure each can be successfully mapped and returned.
     * 
     * @param status the status to test
     */
    @ParameterizedTest
    @ArgumentsSource(value = StatusTypes.class)
    void getStatusByCode_success(CommitterPaperworkElectionStatus status) {
        Optional<CommitterPaperworkElectionStatus> result = CommitterPaperworkElectionStatus
                .getStatusByCode(status.getCode());
        Assertions.assertTrue(result.isPresent());
        Assertions.assertEquals(status, result.get());
    }

    @Test
    void getStatusByCode_failure_noSuchCode() {
        Assertions.assertTrue(CommitterPaperworkElectionStatus.getStatusByCode(-1).isEmpty());
    }

    /**
     * Arg provider that returns paperwork status Types that are recorded by the database on submission.
     * 
     * @author Martin Lowe
     *
     */
    public static class StatusTypes implements ArgumentsProvider {
        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
            return Stream.of(CommitterPaperworkElectionStatus.values()).map(Arguments::of);
        }
    }
}
