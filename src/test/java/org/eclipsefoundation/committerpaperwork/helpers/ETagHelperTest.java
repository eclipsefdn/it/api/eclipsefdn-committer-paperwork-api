/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.helpers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Simple tests to validate the etag functionality.
 * 
 * @author Martin Lowe
 *
 */
@QuarkusTest
class ETagHelperTest {

    @Test
    void generateEtag_success() {
        String input = "sample";
        String output = ETagHelper.generateEtag(input);
        Assertions.assertNotNull(output);
        Assertions.assertNotEquals(input, output);
    }

    @Test
    void generateEtag_success_consistentHashing() {
        String input = "sample-2";
        String output = ETagHelper.generateEtag(input);
        Assertions.assertEquals(output, ETagHelper.generateEtag(input));
    }

    @Test
    void generateEtag_failure_rejectsEmptyContent() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> ETagHelper.generateEtag(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> ETagHelper.generateEtag(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> ETagHelper.generateEtag(" "));
    }
}
