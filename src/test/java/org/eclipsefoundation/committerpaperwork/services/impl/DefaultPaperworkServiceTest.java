/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.services.impl;

import java.net.URI;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.committerpaperwork.daos.EclipsePersistenceDao;
import org.eclipsefoundation.committerpaperwork.dtos.eclipse.AccountRequests;
import org.eclipsefoundation.committerpaperwork.dtos.eclipseapi.CommitterPaperwork;
import org.eclipsefoundation.committerpaperwork.models.ProvisioningRequestData;
import org.eclipsefoundation.committerpaperwork.models.RetirementRequestData;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkParameterNames;
import org.eclipsefoundation.committerpaperwork.services.PaperworkService;
import org.eclipsefoundation.committerpaperwork.test.apis.MockFoundationDBAPI;
import org.eclipsefoundation.committerpaperwork.test.helpers.MockHelper;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.http.helper.IPParser;
import org.eclipsefoundation.http.model.FlatRequestWrapper;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

/**
 * @author Martin Lowe
 *
 */
@QuarkusTest
class DefaultPaperworkServiceTest {

    @Inject
    PaperworkService paperworkService;

    @Inject
    EclipsePersistenceDao eclipseDao;
    @Inject
    FilterService filters;

    @RestClient
    MockFoundationDBAPI fdbAPI;

    @InjectMock
    IPParser parser;
    @InjectMock
    RequestWrapper wrap;

    @BeforeEach
    public void before() {
        Mockito.when(parser.getBestMatchingIP()).thenReturn("0.0.0.0");
        // required, as w/o a request active, this throws an illegal state exception
        Mockito.when(wrap.asMap()).thenReturn(new MultivaluedHashMap<>());
    }

    /*
     * Retirement tests
     */

    @Test
    void retireUser_success_cm() {
        // set up the basic test case
        String targetUser = "opearson";
        String targetProject = "re.tire2";
        String requestorName = "da_wizz";
        String requestorMail = "code.wiz@important.co";

        // set up some of the associated test resources
        EfUser requestor = generateSampleEfUser(requestorName, requestorMail);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_REMOVAL_VALUE);

        // validate pre-test state
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .anyMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected CC relation, but none was found");
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() == null),
                        "Expected people project entry, none found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());

        // do the action
        paperworkService
                .retireUser(RetirementRequestData
                        .builder()
                        .setInfo(RetirementRequestData.Parameters
                                .builder()
                                .setProjectId(targetProject)
                                .setProjectRole("CM")
                                .setRequestor(requestorName)
                                .build())
                        .build(), targetUser, requestor);

        // validate through mock API props
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() != null),
                        "Expected expired people project entry, none found");
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .noneMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected removed CC relation, but was found");
        Assertions
                .assertFalse(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
    }

    @Test
    void retireUser_success_pl() {
        // set up the basic test case
        String targetUser = "grunt";
        String requestorName = "barshal_blathers";
        String requestorMail = "slom@eclipse-foundation.org";
        String targetProject = "re.tire1";

        // set up some of the associated test resources
        EfUser requestor = generateSampleEfUser(requestorName, requestorMail);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_REMOVAL_VALUE);

        // validate pre-test state
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .anyMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected CC relation, but none was found");
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() == null),
                        "Expected people project entry, none found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
        // do the action
        paperworkService
                .retireUser(RetirementRequestData
                        .builder()
                        .setInfo(RetirementRequestData.Parameters
                                .builder()
                                .setProjectId(targetProject)
                                .setProjectRole("PL")
                                .setRequestor(requestorName)
                                .build())
                        .build(), targetUser, requestor);

        // validate through mock API props
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() != null),
                        "Expected expired people project entry, none found");
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .noneMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected removed CC relation, but was found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
    }

    // iss #33 regression test
    @Test
    void retireUser_success_plThenCM() {
        // set up the basic test case
        String targetUser = "oouilson";
        String targetProject = "re.tire4";
        String requestorName = "barshal_blathers";
        String requestorMail = "slom@eclipse-foundation.org";

        // set up some of the associated test resources
        EfUser requestor = generateSampleEfUser(requestorName, requestorMail);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_REMOVAL_VALUE);

        // validate pre-test state
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .anyMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected CC relation, but none was found");
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() == null),
                        "Expected people project entry, none found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
        // do the actions
        paperworkService
                .retireUser(RetirementRequestData
                        .builder()
                        .setInfo(RetirementRequestData.Parameters
                                .builder()
                                .setProjectId(targetProject)
                                .setProjectRole("CM")
                                .setRequestor(requestorName)
                                .build())
                        .build(), targetUser, requestor);
        paperworkService
                .retireUser(RetirementRequestData
                        .builder()
                        .setInfo(RetirementRequestData.Parameters
                                .builder()
                                .setProjectId(targetProject)
                                .setProjectRole("PL")
                                .setRequestor(requestorName)
                                .build())
                        .build(), targetUser, requestor);

        // validate through mock API props
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() != null),
                        "Expected expired people project entry, none found");
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .noneMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected removed CC relation, but was found");
        Assertions
                .assertFalse(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
    }

    // iss #34 regression test
    @Test
    void retireUser_success_inactiveUserRetirement() {
        // set up the basic test case, no EF user assoc. w/ username
        String targetUser = "oouilson_inactive";
        String targetProject = "re.tire4";
        String requestorName = "barshal_blathers";
        String requestorMail = "slom@eclipse-foundation.org";

        // set up some of the associated test resources
        EfUser requestor = generateSampleEfUser(requestorName, requestorMail);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_REMOVAL_VALUE);

        // validate pre-test state
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .anyMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected CC relation, but none was found");
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() == null),
                        "Expected people project entry, none found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
        // do the actions
        paperworkService
                .retireUser(RetirementRequestData
                        .builder()
                        .setInfo(RetirementRequestData.Parameters
                                .builder()
                                .setProjectId(targetProject)
                                .setProjectRole("CM")
                                .setRequestor(requestorName)
                                .build())
                        .build(), targetUser, requestor);

        // validate through mock API props
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() != null),
                        "Expected expired people project entry, none found");
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .noneMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected removed CC relation, but was found");
        Assertions
                .assertFalse(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
    }

    @Test
    void retireUser_success_hasOtherProjects() {
        // set up the basic test case
        String targetUser = "da_wizz";
        String requestorName = "barshal_blathers";
        String requestorMail = "slom@eclipse-foundation.org";
        String targetProject = "re.tire1";

        // set up some of the associated test resources
        EfUser requestor = generateSampleEfUser(requestorName, requestorMail);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_REMOVAL_VALUE);

        // validate pre-test state
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .anyMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected CC relation, but none was found");
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() == null),
                        "Expected people project entry, none found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());

        // set up a basic valid case
        RetirementRequestData request = RetirementRequestData
                .builder()
                .setInfo(RetirementRequestData.Parameters
                        .builder()
                        .setProjectId(targetProject)
                        .setProjectRole("PL")
                        .setRequestor(requestorName)
                        .build())
                .build();
        // do the action
        paperworkService.retireUser(request, targetUser, requestor);

        // validate through mock API props
        Assertions
                .assertTrue(fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() != null),
                        "Expected expired people project entry, none found");
        Assertions
                .assertFalse(
                        fdbAPI.contacts
                                .stream()
                                .noneMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("CC")),
                        "Expected CC relation, but none was found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
    }

    /**
     * Case only happens with CM permset
     */
    @Test
    void retireUser_failure_repeatRequest() {
        // set up the basic test case
        String targetUser = "da_wizz";
        String requestorName = "barshal_blathers";
        String requestorMail = "slom@eclipse-foundation.org";
        String targetProject = "re.tire2";

        // set up some of the associated test resources
        EfUser requestor = generateSampleEfUser(requestorName, requestorMail);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_REMOVAL_VALUE);
        RetirementRequestData request = RetirementRequestData
                .builder()
                .setInfo(RetirementRequestData.Parameters
                        .builder()
                        .setProjectId(targetProject)
                        .setProjectRole("CM")
                        .setRequestor(requestorName)
                        .build())
                .build();

        // pre-test to ensure that nothing exists before call
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());

        // do the action
        paperworkService.retireUser(request, targetUser, requestor);
        // check that the request was tracked
        Assertions
                .assertFalse(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
        // try again, but expect the exception, and check the status code
        Assertions
                .assertEquals(Response.Status.CONFLICT.getStatusCode(), Assertions
                        .assertThrows(WebApplicationException.class, () -> paperworkService.retireUser(request, targetUser, requestor))
                        .getResponse()
                        .getStatus(), "Expected 409 status for repeated request, but found different result");
    }

    /**
     * <ul>
     * <li>Case 1: Not in project
     * <li>Case 2: Wrong role submitted
     * <li>Case 3: Committer is already retired/inactive on project
     * </ul>
     * 
     * @param targetUser the user to retire
     * @param targetUserMail the user to retires email
     * @param requestorName the user requesting the retirement
     * @param requestorMail the requesting users email
     * @param targetProject the project to remove the user from
     */
    @ParameterizedTest
    @CsvSource({ "doofenshmirtz, doofenshmirtz@evil.inc, barshal_blathers, slom@eclipse-foundation.org, tech.nology",
            "barshal_blathers, slom@eclipse-foundation.org, barshal_blathers, slom@eclipse-foundation.org, re.tire1",
            "doofenshmirtz, doofenshmirtz@evil.inc, barshal_blathers, slom@eclipse-foundation.org, sample.platypus" })
    void retireUser_failure_doesNotValidate(String targetUser, String targetUserMail, String requestorName, String requestorMail,
            String targetProject) {
        // set up some of the associated test resources
        EfUser requestor = generateSampleEfUser(requestorName, requestorMail);
        RetirementRequestData request = RetirementRequestData
                .builder()
                .setInfo(RetirementRequestData.Parameters
                        .builder()
                        .setProjectId(targetProject)
                        .setProjectRole("CM")
                        .setRequestor(requestorName)
                        .build())
                .build();

        // do the action, asserting that it will throw due to project error
        Assertions.assertThrows(BadRequestException.class, () -> paperworkService.retireUser(request, targetUser, requestor));
    }

    /*
     * Provisioning tests
     */

    @Test
    void provisionUser_success() {
        String targetUser = "newbie";
        String targetUserMail = "newbie@important.co";
        String targetProject = "tech.nology";
        int targetOrganization = 1;

        // set up some of the associated test resources
        EfUser user = MockHelper.generateSampleEfUser(targetUser, targetUserMail, targetOrganization);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_ADD_VALUE);

        // validate pre-test state
        Assertions
                .assertTrue(fdbAPI.peopleProjects.get(targetUser) == null || fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .noneMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() == null),
                        "Expected no people project entry, one was found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());

        // run the actual test
        paperworkService
                .provisionUser(ProvisioningRequestData
                        .builder()
                        .setOrganization(ProvisioningRequestData.Parameters.builder().setId(targetOrganization).build())
                        .build(), generatePaperwork(targetProject, targetUser), user);

        // check the post test assertions
        Assertions
                .assertTrue(
                        fdbAPI.peopleProjects
                                .get(targetUser)
                                .stream()
                                .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                        && ppd.inactiveDate() == null && ppd.relation().equals("CM")),
                        "Expected people project entry, none found");
        Assertions
                .assertFalse(
                        fdbAPI.contacts
                                .stream()
                                .noneMatch(contact -> contact.personID().equals(targetUser)
                                        && contact.organizationID() == targetOrganization && contact.relation().equalsIgnoreCase("EMPLY")),
                        "Expected EMPLY for target org relation, but none was found");
        Assertions
                .assertFalse(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
    }

    @Test
    void provisionUser_success_changeOrg() {
        String targetUser = "doofenshmirtz";
        String targetUserMail = "doofenshmirtz@evil.inc";
        String targetProject = "tech.nology";
        int targetOrganization = 2;
        int previousOrg = 1;

        // set up some of the associated test resources
        EfUser user = MockHelper.generateSampleEfUser(targetUser, targetUserMail, targetOrganization);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_ADD_VALUE);

        // validate pre-test state
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .anyMatch(contact -> contact.personID().equals(targetUser) && contact.organizationID() == previousOrg
                                        && contact.relation().equalsIgnoreCase("EMPLY")),
                        "Expected no EMPLY for target org relation, but none was found");
        Assertions
                .assertTrue(fdbAPI.peopleProjects.get(targetUser) == null || fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .noneMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() == null),
                        "Expected no people project entry, one was found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());

        // run the actual test
        paperworkService
                .provisionUser(ProvisioningRequestData
                        .builder()
                        .setOrganization(ProvisioningRequestData.Parameters.builder().setId(targetOrganization).build())
                        .build(), generatePaperwork(targetProject, targetUser), user);

        // check the post test assertions
        Assertions
                .assertFalse(
                        fdbAPI.contacts
                                .stream()
                                .anyMatch(contact -> contact.personID().equals(targetUser) && contact.organizationID() == previousOrg
                                        && contact.relation().equalsIgnoreCase("EMPLY")),
                        "Expected no EMPLY for previous org relation, but one was found");
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .anyMatch(contact -> contact.personID().equals(targetUser) && contact.organizationID() == targetOrganization
                                        && contact.relation().equalsIgnoreCase("EMPLY")),
                        "Expected EMPLY for target org relation, but none was found");
        Assertions
                .assertTrue(
                        fdbAPI.peopleProjects
                                .get(targetUser)
                                .stream()
                                .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                        && ppd.inactiveDate() == null && ppd.relation().equals("CM")),
                        "Expected people project entry, none found");
        Assertions
                .assertFalse(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
    }

    @Test
    void provisionUser_success_notInMemberOrg() {
        String targetUser = "somebody";
        String targetUserMail = "somebody@important.co";
        String targetProject = "tech.nology";
        int targetOrg = 0;

        // set up some of the associated test resources
        EfUser user = MockHelper.generateSampleEfUser(targetUser, targetUserMail, null);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_ADD_VALUE);

        // validate pre-test state
        Assertions
                .assertTrue(fdbAPI.contacts
                        .stream()
                        .noneMatch(contact -> contact.personID().equals(targetUser) && contact.relation().equalsIgnoreCase("EMPLY")),
                        "Expected no EMPLY for target org relation, but one was found");
        Assertions
                .assertTrue(fdbAPI.peopleProjects.get(targetUser) == null || fdbAPI.peopleProjects
                        .get(targetUser)
                        .stream()
                        .noneMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                && ppd.inactiveDate() == null),
                        "Expected no people project entry, one was found");
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());

        // run the actual test
        paperworkService
                .provisionUser(ProvisioningRequestData
                        .builder()
                        .setOrganization(ProvisioningRequestData.Parameters.builder().setId(targetOrg).build())
                        .build(), generatePaperwork(targetProject, targetUser), user);

        // check the post test assertions
        Assertions
                .assertFalse(
                        fdbAPI.contacts
                                .stream()
                                .anyMatch(contact -> contact.personID().equals(targetUser) && contact.organizationID() == targetOrg
                                        && contact.relation().equalsIgnoreCase("EMPLY")),
                        "Expected no EMPLY for previous org relation, but one was found");
        Assertions
                .assertTrue(
                        fdbAPI.contacts
                                .stream()
                                .noneMatch(contact -> contact.personID().equals(targetUser) && contact.organizationID() == targetOrg
                                        && contact.relation().equalsIgnoreCase("EMPLY")),
                        "Expected no EMPLY for target org relation, but one was found");
        Assertions
                .assertTrue(
                        fdbAPI.peopleProjects
                                .get(targetUser)
                                .stream()
                                .anyMatch(ppd -> ppd.personID().equals(targetUser) && ppd.projectID().equals(targetProject)
                                        && ppd.inactiveDate() == null && ppd.relation().equals("CM")),
                        "Expected people project entry, none found");
        Assertions
                .assertFalse(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
    }

    @Test
    void provisionUser_failure_repeatRequest() {
        // set up the basic test case
        String targetUser = "testerson";
        String targetUserMail = "testerson@somewhere.ca";
        String targetProject = "re.tire2";
        int targetOrganization = 3;

        // set up some of the associated test resources
        EfUser user = generateSampleEfUser(targetUser, targetUserMail);
        MultivaluedMap<String, String> params = createAccountRequestParams(targetUser, targetProject,
                DefaultPaperworkService.GROUP_ADD_VALUE);

        // pre-test to ensure that nothing exists before call
        Assertions
                .assertTrue(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());

        ProvisioningRequestData request = ProvisioningRequestData
                .builder()
                .setOrganization(ProvisioningRequestData.Parameters.builder().setId(targetOrganization).build())
                .build();
        CommitterPaperwork papers = generatePaperwork(targetProject, targetUser);
        // do the action the first time
        paperworkService.provisionUser(request, papers, user);

        // check that the request was tracked
        Assertions
                .assertFalse(eclipseDao
                        .get(new RDBMSQuery<>(new FlatRequestWrapper(URI.create("http://eclipse.org")), filters.get(AccountRequests.class),
                                params))
                        .isEmpty());
        // try again, but expect the exception, and check the status code
        Assertions
                .assertEquals(Response.Status.CONFLICT.getStatusCode(),
                        Assertions
                                .assertThrows(WebApplicationException.class, () -> paperworkService.provisionUser(request, papers, user))
                                .getResponse()
                                .getStatus(),
                        "Expected 409 status for repeated request, but found different result");
    }

    private MultivaluedMap<String, String> createAccountRequestParams(String username, String targetProject, String operation) {
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(CommitterPaperworkParameterNames.USERNAME_NAME_RAW, username);
        params.add(CommitterPaperworkParameterNames.TOKEN_NAME_RAW, targetProject);
        params.add(CommitterPaperworkParameterNames.PERMISSION_OPERATION_NAME_RAW, operation);
        return params;
    }

    private CommitterPaperwork generatePaperwork(String projectId, String uid) {
        // doesn't need to be persisted for these tests
        CommitterPaperwork papers = new CommitterPaperwork();
        papers.setCommitterPaperworkNid(0);
        papers.setCommitterPaperworkStatus(3);
        papers.setCreated(0);
        papers.setChanged(0);
        papers.setElectionNid(0);
        papers.setElectionStatus(2);
        papers.setEtag("");
        papers.setForge("eclipse");
        papers.setId(0);
        papers.setProjectId(projectId);
        papers.setSpecProjectWorkingGroupTid(0);
        papers.setUid(uid);
        return papers;
    }

    private EfUser generateSampleEfUser(String username, String email) {
        return MockHelper.generateSampleEfUser(username, email, null);
    }
}
