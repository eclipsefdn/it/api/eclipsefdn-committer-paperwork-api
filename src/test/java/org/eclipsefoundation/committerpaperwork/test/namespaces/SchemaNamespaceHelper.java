/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.committerpaperwork.test.namespaces;

public final class SchemaNamespaceHelper {
    public static final String BASE_SCHEMAS_PATH = "schemas/";
    public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";

    public static final String PAPERWORK_LIST_SCHEMA_PATH = BASE_SCHEMAS_PATH + "paperwork-list"
            + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String PAPERWORK_SCHEMA_PATH = BASE_SCHEMAS_PATH + "paperwork" + BASE_SCHEMAS_PATH_SUFFIX;
    public static final String RESULT_URL_SCHEMA_PATH = BASE_SCHEMAS_PATH + "result-url" + BASE_SCHEMAS_PATH_SUFFIX;

    public static final String ERROR_SCHEMA_PATH = BASE_SCHEMAS_PATH + "error" + BASE_SCHEMAS_PATH_SUFFIX;
}
