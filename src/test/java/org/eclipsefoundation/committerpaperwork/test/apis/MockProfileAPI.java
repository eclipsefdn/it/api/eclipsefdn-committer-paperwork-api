/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.test.apis;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.committerpaperwork.test.helpers.MockHelper;
import org.eclipsefoundation.efservices.api.ProfileAPI;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUserBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserEcaBuilder;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Alternative;
import jakarta.inject.Singleton;

/**
 * @author martin
 *
 */
@Alternative
@Priority(1)
@RestClient
@Singleton
public class MockProfileAPI implements ProfileAPI {

    public List<EfUser> users;

    @PostConstruct
    void init() {
        this.users = new LinkedList<>();
        users
                .add(EfUserBuilder.builder(MockHelper.generateSampleEfUser("barshall_blathers", "slom@eclipse-foundation.org", null))
                        .isCommitter(false)
                        .uid("0")
                        .firstName("Barshall")
                        .lastName("Blathers")
                        .eca(EfUserEcaBuilder.builder().canContributeSpecProject(true).signed(true).build())
                        .build());
        users
                .add(EfUserBuilder.builder(MockHelper.generateSampleEfUser("da_wizz", "code.wiz@important.co", null))
                        .isCommitter(true)
                        .uid("1")
                        .firstName("Da")
                        .lastName("Wizz")
                        .eca(EfUserEcaBuilder.builder().canContributeSpecProject(true).signed(true).build())
                        .build());
        users
                .add(EfUserBuilder.builder(MockHelper.generateSampleEfUser("grunt", "grunt@important.co", null))
                        .isCommitter(true)
                        .uid("3")
                        .firstName("Grunt")
                        .lastName("Grunt")
                        .eca(EfUserEcaBuilder.builder().canContributeSpecProject(true).signed(true).build())
                        .build());
        users
                .add(EfUserBuilder.builder(MockHelper.generateSampleEfUser("opearson", "opearson@sample.co", null))
                        .isCommitter(true)
                        .uid("2")
                        .firstName("Oli")
                        .lastName("Pearson")
                        .eca(EfUserEcaBuilder.builder().canContributeSpecProject(true).signed(true).build())
                        .build());
        users
                .add(EfUserBuilder.builder(MockHelper.generateSampleEfUser("newbie", "newbie@important.co", null))
                        .isCommitter(true)
                        .uid("5")
                        .firstName("Newbie")
                        .lastName("Nobody")
                        .eca(EfUserEcaBuilder.builder().canContributeSpecProject(true).signed(true).build())
                        .build());
        users
                .add(EfUserBuilder.builder(MockHelper.generateSampleEfUser("doofenshmirtz", "doofenshmirtz@evil.inc", null))
                        .isCommitter(true)
                        .uid("6")
                        .firstName("Dr")
                        .lastName("Doofenshmirtz")
                        .eca(EfUserEcaBuilder.builder().canContributeSpecProject(true).signed(true).build())
                        .build());
        users
                .add(EfUserBuilder.builder(MockHelper.generateSampleEfUser("somebody", "somebody@important.co", null))
                        .isCommitter(false)
                        .uid("6")
                        .firstName("Somebody")
                        .lastName("Fancy")
                        .eca(EfUserEcaBuilder.builder().canContributeSpecProject(true).signed(true).build())
                        .build());

    }

    @Override
    public List<EfUser> getUsers(String token, UserSearchParams arg1) {
        return users
                .stream()
                .filter(u -> StringUtils.isBlank(arg1.mail) || u.mail().equals(arg1.mail))
                .filter(u -> StringUtils.isBlank(arg1.name) || u.name().equals(arg1.name))
                .filter(u -> arg1.uid == null || u.uid() == arg1.uid)
                .toList();
    }

    @Override
    public EfUser getUserByEfUsername(String token, String username) {
        if (StringUtils.isBlank(username)) {
            return null;
        }
        return users.stream().filter(u -> username.equals(u.name())).findFirst().orElse(null);
    }

    @Override
    public EfUser getUserByGithubHandle(String token, String handle) {
        if (StringUtils.isBlank(handle)) {
            return null;
        }
        return users.stream().filter(u -> handle.equals(u.githubHandle())).findFirst().orElse(null);
    }
}