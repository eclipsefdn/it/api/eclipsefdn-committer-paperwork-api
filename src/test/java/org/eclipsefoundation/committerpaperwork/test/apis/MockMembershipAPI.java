/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.test.apis;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.committerpaperwork.apis.MembershipAPI;
import org.eclipsefoundation.committerpaperwork.apis.models.SlimMemberOrganization;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.jboss.resteasy.reactive.RestResponse;

import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Alternative;
import jakarta.inject.Singleton;

/**
 * Simple mock of membership data for tests
 * 
 * @author Martin Lowe
 *
 */
@Alternative
@Priority(1)
@RestClient
@Singleton
public class MockMembershipAPI implements MembershipAPI {

    public List<SlimMemberOrganization> orgs;

    public MockMembershipAPI() {
        this.orgs = new ArrayList<>();
        this.orgs.add(generateOrg(1, "Sample org 1"));
        this.orgs.add(generateOrg(2, "Sample org 2"));
        this.orgs.add(generateOrg(3, "Sample org 3"));
    }

    @Override
    public RestResponse<List<SlimMemberOrganization>> getOrganizations(BaseAPIParameters params) {
        return RestResponse.ok(new ArrayList<>(orgs));
    }

    private SlimMemberOrganization generateOrg(int id, String name) {
        return SlimMemberOrganization
                .builder()
                .setLogos(SlimMemberOrganization.MemberOrganizationLogos.builder().setPrint("").setWeb("").build())
                .setName(name)
                .setOrganizationID(id)
                .build();
    }
}
