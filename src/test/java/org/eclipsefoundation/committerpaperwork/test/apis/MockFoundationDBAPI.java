/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.test.apis;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.committerpaperwork.apis.FoundationDBAPI;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogData;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.jboss.resteasy.reactive.RestResponse;

import jakarta.annotation.Priority;
import jakarta.enterprise.inject.Alternative;
import jakarta.inject.Singleton;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * @author martin
 *
 */
@Alternative
@Priority(1)
@RestClient
@Singleton
public class MockFoundationDBAPI implements FoundationDBAPI {
    public List<OrganizationContactData> contacts;
    public Map<String, PeopleData> people;
    public MultivaluedMap<String, PeopleProjectData> peopleProjects;
    public List<ProjectData> projects;
    public List<SysModLogData> submittedLogs;

    public MockFoundationDBAPI() {

        // organizations setup
        this.contacts = new ArrayList<>();
        this.contacts.add(generateOrgContact(1, "da_wizz", "EMPLY"));
        this.contacts.add(generateOrgContact(1, "da_wizz", "CC"));
        this.contacts.add(generateOrgContact(2, "barshal_blathers", "EMPLY"));
        this.contacts.add(generateOrgContact(2, "barshal_blathers", "CC"));
        this.contacts.add(generateOrgContact(1, "grunt", "EMPLY"));
        this.contacts.add(generateOrgContact(1, "grunt", "CC"));
        this.contacts.add(generateOrgContact(3, "opearson", "EMPLY"));
        this.contacts.add(generateOrgContact(3, "opearson", "CC"));
        this.contacts.add(generateOrgContact(1, "doofenshmirtz", "EMPLY"));
        this.contacts.add(generateOrgContact(1, "oouilson", "CC"));
        this.contacts.add(generateOrgContact(1, "oouilson_inactive", "CC"));

        // people setup
        this.people = new HashMap<>();
        this.people.put("opearson", createSamplePerson("opearson", "Oli", "Pearson"));
        this.people.put("da_wizz", createSamplePerson("da_wizz", "Da", "Wizz"));
        this.people.put("doofenshmirtz", createSamplePerson("doofenshmirtz", "Dr", "Doofenshmirtz"));
        this.people.put("barshal_blathers", createSamplePerson("barshal_blathers", "Barshal", "Blathers"));
        this.people.put("grunt", createSamplePerson("grunt", "Grunt", "Grunt"));
        this.people.put("newbie", createSamplePerson("newbie", "New", "Bie"));
        this.people.put("oouilson", createSamplePerson("oouilson", "Ouade", "Ouilson"));
        this.people.put("oouilson_inactive", createSamplePerson("oouilson_inactive", "Ouade", "Ouilson"));
        this.peopleProjects = new MultivaluedHashMap<>();
        this.peopleProjects.put("opearson", Collections.emptyList());
        this.peopleProjects
                .put("da_wizz",
                        new ArrayList<>(Arrays
                                .asList(createPeopleProject("da_wizz", "re.tire1", "PL", null),
                                        createPeopleProject("da_wizz", "re.tire2", "CM", null),
                                        createPeopleProject("da_wizz", "sample.project", "PL", null))));
        this.peopleProjects.put("grunt", new ArrayList<>(Arrays.asList(createPeopleProject("grunt", "re.tire1", "PL", null))));
        this.peopleProjects.put("opearson", new ArrayList<>(Arrays.asList(createPeopleProject("opearson", "re.tire2", "CM", null))));
        this.peopleProjects
                .put("doofenshmirtz",
                        new ArrayList<>(Arrays
                                .asList(createPeopleProject("doofenshmirtz", "sample.platypus", "CM",
                                        Date.from(DateTimeHelper.now().minus(2, ChronoUnit.MONTHS).toInstant())))));
        this.peopleProjects
                .put("barshal_blathers",
                        new ArrayList<>(Arrays
                                .asList(createPeopleProject("barshal_blathers", "sample.project", "PL", null),
                                        createPeopleProject("barshal_blathers", "tech.nology", "CM", null))));
        this.peopleProjects
                .put("oouilson",
                        new ArrayList<>(Arrays
                                .asList(createPeopleProject("oouilson", "re.tire4", "PL", null),
                                        createPeopleProject("oouilson", "re.tire4", "CM", null))));
        this.peopleProjects
                .put("oouilson_inactive", new ArrayList<>(Arrays.asList(createPeopleProject("oouilson_inactive", "re.tire4", "CM", null))));

        // projects setup
        this.projects = new LinkedList<>();
        this.projects.add(buildSampleProject("sample.project", "The coolest thing (TM)"));
        this.projects.add(buildSampleProject("sample.platypus", "Perry the sample project?!"));
        this.projects.add(buildSampleProject("tech.nology", "TECHnology"));
        this.projects.add(buildSampleProject("re.tire1", "Retirement test1"));
        this.projects.add(buildSampleProject("re.tire2", "Retirement test2"));
        this.projects.add(buildSampleProject("re.tire3", "Retirement test3"));
        this.projects.add(buildSampleProject("re.tire4", "Retirement test4"));

        // sys setup
        this.submittedLogs = new LinkedList<>();
    }

    @Override
    public RestResponse<List<OrganizationContactData>> getOrganzationContacts(BaseAPIParameters params, String personId, String relation) {
        return RestResponse
                .ok(contacts
                        .stream()
                        .filter(oc -> oc.personID().equalsIgnoreCase(personId)
                                && (StringUtils.isBlank(relation) || oc.relation().equalsIgnoreCase(relation)))
                        .toList());
    }

    @Override
    public OrganizationContactData updateOrganizationContact(Integer organizationId, OrganizationContactData contact) {
        IntStream
                .range(0, contacts.size() - 1)
                .filter(i -> contacts.get(i).personID().equals(contact.personID()) && contacts.get(i).organizationID() == organizationId)
                .findFirst()
                .ifPresentOrElse(i -> contacts.set(i, contact), () -> contacts.add(contact));
        return contact;
    }

    @Override
    public RestResponse<Void> removeOrganizationContact(Integer organizationId, String personId, String relation) {
        OptionalInt idx = IntStream
                .range(0, contacts.size())
                .filter(i -> contacts.get(i).personID().equals(personId) && contacts.get(i).organizationID() == organizationId
                        && contacts.get(i).relation().equalsIgnoreCase(relation))
                .findFirst();
        idx.ifPresent(i -> contacts.remove(i));
        return idx.isPresent() ? RestResponse.ok() : RestResponse.status(404);
    }

    @Override
    public PeopleData getPersonalRecord(String personId) {
        return people.get(personId);
    }

    @Override
    public RestResponse<List<PeopleData>> updatePersonalRecord(PeopleData personRecord) {
        people.put(personRecord.personID(), personRecord);
        return RestResponse.ok(Arrays.asList(people.get(personRecord.personID())));
    }

    @Override
    public RestResponse<List<PeopleProjectData>> getPeopleProjects(BaseAPIParameters params, String username) {
        return RestResponse.ok(peopleProjects.containsKey(username) ? peopleProjects.get(username) : Collections.emptyList());
    }

    @Override
    public RestResponse<PeopleProjectData> updatePeopleProjectEntry(String username, PeopleProjectData updatedEntry) {
        List<PeopleProjectData> data = peopleProjects.getOrDefault(username, new ArrayList<>());
        if (data.isEmpty()) {
            data.add(updatedEntry);
        } else {
            IntStream.range(0, data.size()).filter(i -> {
                PeopleProjectData o = data.get(i);
                return o.projectID().equalsIgnoreCase(updatedEntry.projectID()) && o.relation().equalsIgnoreCase(updatedEntry.relation());
            }).findFirst().ifPresentOrElse(i -> data.set(i, updatedEntry), () -> data.add(updatedEntry));
        }
        peopleProjects.put(updatedEntry.personID(), data);
        return RestResponse.ok(updatedEntry);
    }

    @Override
    public ProjectData getProjects(String projectId) {
        return this.projects.stream().filter(p -> p.projectID().equalsIgnoreCase(projectId)).findFirst().orElse(null);
    }

    @Override
    public RestResponse<List<SysModLogData>> addModLog(SysModLogData newLogData) {
        // lock on the object and add the log to the internal list
        synchronized (this) {
            this.submittedLogs.add(newLogData);
        }
        return RestResponse.ok(Arrays.asList(newLogData));
    }

    public List<SysModLogData> getSubmittedLogs() {
        synchronized (this) {
            return new LinkedList<>(this.submittedLogs);
        }
    }

    private ProjectData buildSampleProject(String projectId, String name) {
        return new ProjectData(projectId, name, 2, "eclipse", "description", "", "", new Date(), 0, true, "", "Active", 4, false, false);
    }

    private OrganizationContactData generateOrgContact(Integer organizationId, String username, String relation) {
        return new OrganizationContactData(organizationId, username, relation, "", "", "Sample");
    }

    private PeopleData createSamplePerson(String uname, String fname, String lname) {
        return new PeopleData(uname, fname, lname, "XX", false, "", "", "", "", "", "", false, false, "", new Date(), null, null, null,
                null);
    }

    private PeopleProjectData createPeopleProject(String personId, String projectId, String relation, Date inactiveDate) {
        return new PeopleProjectData(projectId, personId, relation, new Date(), inactiveDate, false);
    }
}
