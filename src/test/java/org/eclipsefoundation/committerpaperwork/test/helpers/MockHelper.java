/*********************************************************************
* Copyright (c) 2025 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.committerpaperwork.test.helpers;

import java.util.Collections;

import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.EfUserBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserCountryBuilder;
import org.eclipsefoundation.efservices.api.models.EfUserEcaBuilder;

/**
 * 
 */
public final class MockHelper {

    public static EfUser generateSampleEfUser(String username, String email, Integer orgId) {
        return EfUserBuilder
                .builder()
                .firstName("")
                .lastName("")
                .fullName("")
                .isBot(false)
                .isCommitter(false)
                .mail(email)
                .picture("")
                .publisherAgreements(Collections.emptyMap())
                .twitterHandle("")
                .org("")
                .jobTitle("")
                .website("")
                .country(EfUserCountryBuilder.builder().code("CA").name("Canada").build())
                .interests(Collections.emptyList())
                .name(username)
                .orgId(orgId)
                .uid("0")
                .eca(EfUserEcaBuilder.builder().signed(true).canContributeSpecProject(false).build())
                .build();
    }
}
