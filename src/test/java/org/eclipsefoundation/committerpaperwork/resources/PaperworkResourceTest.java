/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.resources;

import static org.eclipsefoundation.committerpaperwork.test.namespaces.SchemaNamespaceHelper.PAPERWORK_LIST_SCHEMA_PATH;
import static org.eclipsefoundation.committerpaperwork.test.namespaces.SchemaNamespaceHelper.PAPERWORK_SCHEMA_PATH;
import static org.eclipsefoundation.committerpaperwork.test.namespaces.SchemaNamespaceHelper.RESULT_URL_SCHEMA_PATH;

import java.util.Optional;

import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import org.eclipsefoundation.committerpaperwork.models.PaperworkUpdateData;
import org.eclipsefoundation.committerpaperwork.models.ProspectivePaperworkData;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkAuthScopes;
import org.eclipsefoundation.testing.helpers.AuthHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;

/**
 * @author Martin Lowe
 *
 */
@QuarkusTest
class PaperworkResourceTest {

    public static final String COMMITTER_PAPERWORK_ROOT_URL = "";
    public static final String COMMITTER_PAPERWORK_FOR_USER_ENDPOINT = COMMITTER_PAPERWORK_ROOT_URL + "/{username}";
    public static final String COMMITTER_PAPERWORK_SINGULAR_ENDPOINT = COMMITTER_PAPERWORK_FOR_USER_ENDPOINT + "/{id}";

    private static final EndpointTestCase GET_PAPERWORK_LIST_SUCCESS = TestCaseHelper
            .buildSuccessCase(COMMITTER_PAPERWORK_ROOT_URL, new String[] {}, PAPERWORK_LIST_SCHEMA_PATH);
    private static final EndpointTestCase GET_PAPERWORK_LIST_UNAUTHENTICATED = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_ROOT_URL, new String[] {}, PAPERWORK_LIST_SCHEMA_PATH)
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();
    private static final EndpointTestCase GET_PAPERWORK_LIST_FORBIDDEN = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_ROOT_URL, new String[] {}, PAPERWORK_LIST_SCHEMA_PATH)
            .setStatusCode(Status.FORBIDDEN.getStatusCode())
            .build();

    private static final EndpointTestCase POST_PAPERWORK_SUCCESS = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_FOR_USER_ENDPOINT, new String[] { "da_wizz" }, RESULT_URL_SCHEMA_PATH)
            .setStatusCode(Status.CREATED.getStatusCode())
            .build();
    private static final EndpointTestCase POST_PAPERWORK_FAILURE_UNAUTHENTICATED = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_FOR_USER_ENDPOINT, new String[] { "da_wizz" }, RESULT_URL_SCHEMA_PATH)
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();
    private static final EndpointTestCase POST_PAPERWORK_FAILURE_FORBIDDEN = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_FOR_USER_ENDPOINT, new String[] { "da_wizz" }, RESULT_URL_SCHEMA_PATH)
            .setStatusCode(Status.FORBIDDEN.getStatusCode())
            .build();

    private static final EndpointTestCase ACCESS_PAPERWORK_FOR_USER_SUCCESS = TestCaseHelper
            .buildSuccessCase(COMMITTER_PAPERWORK_SINGULAR_ENDPOINT, new String[] { "da_wizz", "123" },
                    PAPERWORK_SCHEMA_PATH);
    private static final EndpointTestCase ACCESS_PAPERWORK_FOR_USER_UNAUTHENTICATED = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_SINGULAR_ENDPOINT, new String[] { "da_wizz", "123" },
                    PAPERWORK_SCHEMA_PATH)
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();
    private static final EndpointTestCase ACCESS_PAPERWORK_FOR_USER_FORBIDDEN = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_SINGULAR_ENDPOINT, new String[] { "da_wizz", "123" },
                    PAPERWORK_SCHEMA_PATH)
            .setStatusCode(Status.FORBIDDEN.getStatusCode())
            .build();

    private static final EndpointTestCase UPDATE_PAPERWORK_FOR_USER_SUCCESS = TestCaseHelper
            .buildSuccessCase(COMMITTER_PAPERWORK_SINGULAR_ENDPOINT, new String[] { "da_wizz", "123" },
                    RESULT_URL_SCHEMA_PATH);
    private static final EndpointTestCase UPDATE_PAPERWORK_FOR_USER_UNAUTHENTICATED = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_SINGULAR_ENDPOINT, new String[] { "da_wizz", "123" },
                    RESULT_URL_SCHEMA_PATH)
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();
    private static final EndpointTestCase UPDATE_PAPERWORK_FOR_USER_FORBIDDEN = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_SINGULAR_ENDPOINT, new String[] { "da_wizz", "123" },
                    RESULT_URL_SCHEMA_PATH)
            .setStatusCode(Status.FORBIDDEN.getStatusCode())
            .build();

    private static final EndpointTestCase DELETE_PAPERWORK_FOR_USER_SUCCESS = EndpointTestCase
            .builder()
            .setPath(COMMITTER_PAPERWORK_SINGULAR_ENDPOINT)
            .setParams(Optional.of(new String[] { "da_wizz", "100123" }))
            .setStatusCode(Response.Status.NO_CONTENT.getStatusCode())
            .build();
    private static final EndpointTestCase DELETE_PAPERWORK_FOR_USER_UNAUTHENTICATED = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_SINGULAR_ENDPOINT, new String[] { "da_wizz", "123" },
                    RESULT_URL_SCHEMA_PATH)
            .setStatusCode(Status.UNAUTHORIZED.getStatusCode())
            .build();
    private static final EndpointTestCase DELETE_PAPERWORK_FOR_USER_FORBIDDEN = TestCaseHelper
            .prepareTestCase(COMMITTER_PAPERWORK_SINGULAR_ENDPOINT, new String[] { "da_wizz", "123" },
                    RESULT_URL_SCHEMA_PATH)
            .setStatusCode(Status.FORBIDDEN.getStatusCode())
            .build();

    @Inject
    ObjectMapper mapper;

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.READ)
    void getPaperwork_success() {
        EndpointTestBuilder.from(GET_PAPERWORK_LIST_SUCCESS).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.READ)
    void getPaperwork_success_schema() {
        EndpointTestBuilder.from(GET_PAPERWORK_LIST_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getPaperwork_unauthenticated() {
        EndpointTestBuilder.from(GET_PAPERWORK_LIST_UNAUTHENTICATED).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = "some_other_scope")
    void getPaperwork_forbidden_wrongScope() {
        EndpointTestBuilder.from(GET_PAPERWORK_LIST_FORBIDDEN).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.WRITE)
    void createPaperwork_success() {
        EndpointTestBuilder
                .from(POST_PAPERWORK_SUCCESS)
                .doPost(convertToJsonString(ProspectivePaperworkData
                        .builder()
                        .setParameters(ProspectivePaperworkData.Parameters
                                .builder()
                                .setElectionNid(0)
                                .setElectionStatus(2)
                                .setForge("eclipse")
                                .setProjectId("sample.platypus")
                                .build())
                        .build()))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.WRITE)
    void createPaperwork_success_schema() {
        EndpointTestBuilder
                .from(POST_PAPERWORK_SUCCESS)
                .doPost(convertToJsonString(ProspectivePaperworkData
                        .builder()
                        .setParameters(ProspectivePaperworkData.Parameters
                                .builder()
                                .setElectionNid(0)
                                .setElectionStatus(2)
                                .setForge("eclipse")
                                .setProjectId("iot.sparkplug")
                                .build())
                        .build()))
                .andCheckSchema()
                .run();
    }

    @Test
    void createPaperwork_unauthenticated() {
        EndpointTestBuilder
                .from(POST_PAPERWORK_FAILURE_UNAUTHENTICATED)
                .doPost(convertToJsonString(ProspectivePaperworkData
                        .builder()
                        .setParameters(ProspectivePaperworkData.Parameters
                                .builder()
                                .setElectionNid(0)
                                .setElectionStatus(2)
                                .setForge("eclipse")
                                .setProjectId("sample.platypus")
                                .build())
                        .build()))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.READ)
    void createPaperwork_forbidden_wrongScope() {
        EndpointTestBuilder
                .from(POST_PAPERWORK_FAILURE_FORBIDDEN)
                .doPost(convertToJsonString(ProspectivePaperworkData
                        .builder()
                        .setParameters(ProspectivePaperworkData.Parameters
                                .builder()
                                .setElectionNid(0)
                                .setElectionStatus(2)
                                .setForge("eclipse")
                                .setProjectId("sample.platypus")
                                .build())
                        .build()))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.READ)
    void getPaperworkForUser_success() {
        EndpointTestBuilder.from(ACCESS_PAPERWORK_FOR_USER_SUCCESS).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.READ)
    void getPaperworkForUser_success_schema() {
        EndpointTestBuilder.from(ACCESS_PAPERWORK_FOR_USER_SUCCESS).andCheckSchema().run();
    }

    @Test
    void getPaperworkForUser_unauthenticated() {
        EndpointTestBuilder.from(ACCESS_PAPERWORK_FOR_USER_UNAUTHENTICATED).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.WRITE)
    void getPaperworkForUser_forbidden_wrongScope() {
        EndpointTestBuilder.from(ACCESS_PAPERWORK_FOR_USER_FORBIDDEN).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.WRITE)
    void updatePaperworkForUser_success() {
        EndpointTestBuilder
                .from(UPDATE_PAPERWORK_FOR_USER_SUCCESS)
                .doPost(convertToJsonString(PaperworkUpdateData
                        .builder()
                        .setParameters(PaperworkUpdateData.Parameters
                                .builder()
                                .setCommitterPaperworkNid(0)
                                .setCommitterPaperworkStatus(0)
                                .build())
                        .build()))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.WRITE)
    void updatePaperworkForUser_success_schema() {
        EndpointTestBuilder
                .from(UPDATE_PAPERWORK_FOR_USER_SUCCESS)
                .doPost(convertToJsonString(PaperworkUpdateData
                        .builder()
                        .setParameters(PaperworkUpdateData.Parameters
                                .builder()
                                .setCommitterPaperworkNid(0)
                                .setCommitterPaperworkStatus(0)
                                .build())
                        .build()))
                .andCheckSchema()
                .run();
    }

    @Test
    void updatePaperworkForUser_unauthenticated() {
        EndpointTestBuilder
                .from(UPDATE_PAPERWORK_FOR_USER_UNAUTHENTICATED)
                .doPost(convertToJsonString(PaperworkUpdateData
                        .builder()
                        .setParameters(PaperworkUpdateData.Parameters
                                .builder()
                                .setCommitterPaperworkNid(0)
                                .setCommitterPaperworkStatus(0)
                                .build())
                        .build()))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.READ)
    void updatePaperworkForUser_forbidden_wrongScope() {
        EndpointTestBuilder
                .from(UPDATE_PAPERWORK_FOR_USER_FORBIDDEN)
                .doPost(convertToJsonString(PaperworkUpdateData
                        .builder()
                        .setParameters(PaperworkUpdateData.Parameters
                                .builder()
                                .setCommitterPaperworkNid(0)
                                .setCommitterPaperworkStatus(0)
                                .build())
                        .build()))
                .run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.DELETE)
    void deletePaperworkForUser_success() {
        EndpointTestBuilder.from(DELETE_PAPERWORK_FOR_USER_SUCCESS).doDelete(null).run();
    }

    @Test
    void deletePaperworkForUser_unauthenticated() {
        EndpointTestBuilder.from(DELETE_PAPERWORK_FOR_USER_UNAUTHENTICATED).doDelete(null).run();
    }

    @Test
    @TestSecurity(user = AuthHelper.TEST_USER_NAME, roles = CommitterPaperworkAuthScopes.WRITE)
    void deletePaperworkForUser_forbidden_wrongScope() {
        EndpointTestBuilder.from(DELETE_PAPERWORK_FOR_USER_FORBIDDEN).doDelete(null).run();
    }

    /**
     * Required for body of requests in tests, as RESTassured doesn't respect the config set for
     * 
     * @param o the object to serialize
     * @return the serialized JSON string
     */
    private String convertToJsonString(Object o) {
        try {
            return mapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
        }
        return null;
    }
}
