/*********************************************************************
* Copyright (c) 2022 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.committerpaperwork.apis.models;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

@AutoValue
@JsonDeserialize(builder = AutoValue_SlimMemberOrganization.Builder.class)
public abstract class SlimMemberOrganization {

    public abstract int getOrganizationID();

    public abstract String getName();

    public abstract MemberOrganizationLogos getLogos();

    public static Builder builder() {
        return new AutoValue_SlimMemberOrganization.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganizationID(int organizationID);

        public abstract Builder setName(String name);

        public abstract Builder setLogos(MemberOrganizationLogos logos);

        public abstract SlimMemberOrganization build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_SlimMemberOrganization_MemberOrganizationLogos.Builder.class)
    public abstract static class MemberOrganizationLogos {
        @Nullable
        public abstract String getPrint();

        @Nullable
        public abstract String getWeb();

        public static Builder builder() {
            return new AutoValue_SlimMemberOrganization_MemberOrganizationLogos.Builder();
        }

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setPrint(@Nullable String print);

            public abstract Builder setWeb(@Nullable String web);

            public abstract MemberOrganizationLogos build();
        }
    }

}
