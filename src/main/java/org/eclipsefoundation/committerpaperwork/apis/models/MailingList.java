/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.apis.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * Model for the Mailing List API, denoting the mailing lists available for a project.
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_MailingList.Builder.class)
public abstract class MailingList {

    public abstract String getEmail();

    public abstract String getProjectId();

    public abstract boolean getIsDisabled();

    public abstract boolean getIsDeleted();

    public static Builder builder() {
        return new AutoValue_MailingList.Builder();
    }

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {

        public abstract Builder setEmail(String email);

        public abstract Builder setProjectId(String projectId);

        public abstract Builder setIsDisabled(boolean isDisabled);

        public abstract Builder setIsDeleted(boolean isDeleted);

        public abstract MailingList build();
    }
}
