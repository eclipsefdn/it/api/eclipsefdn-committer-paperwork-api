/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.apis;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogData;
import org.jboss.resteasy.reactive.RestResponse;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

/**
 * Combined API client for accessing Foundation DB API endpoints.
 * 
 * @author Martin Lowe
 *
 */
@OidcClientFilter
@Path("")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
public interface FoundationDBAPI {

    /* Organization endpoints */
    @GET
    @Path("organizations/contacts")
    RestResponse<List<OrganizationContactData>> getOrganzationContacts(@BeanParam BaseAPIParameters params, @QueryParam("personID") String personId,
            @QueryParam("relation") String relation);

    @PUT
    @Path("organizations/{organization_id}/contacts")
    OrganizationContactData updateOrganizationContact(@PathParam("organization_id") Integer organizationId,
            OrganizationContactData contact);

    @DELETE
    @Path("organizations/{organization_id}/contacts/{person_id}/{relation}")
    RestResponse<Void> removeOrganizationContact(@PathParam("organization_id") Integer organizationId,
            @PathParam("person_id") String personId, @PathParam("relation") String relation);

    /* People endpoints */
    @GET
    @Path("people/{person_id}")
    PeopleData getPersonalRecord(@PathParam("person_id") String personId);

    @PUT
    @Path("people")
    RestResponse<List<PeopleData>> updatePersonalRecord(PeopleData personRecord);

    @GET
    @Path("people/projects")
    RestResponse<List<PeopleProjectData>> getPeopleProjects(@BeanParam BaseAPIParameters params, @QueryParam("personID") String username);

    @PUT
    @Path("people/{person_id}/projects")
    RestResponse<PeopleProjectData> updatePeopleProjectEntry(@PathParam("person_id") String username, PeopleProjectData updatedEntry);

    /* Projects endpoints */
    /**
     * Retrieves the project with the matching project ID.
     * 
     * @param projectId the full project ID to lookup in the Foundation API.
     * @return the project with matching ID if it exists.
     */
    @GET
    @Path("projects/{projectId}")
    ProjectData getProjects(@PathParam("projectId") String projectId);

    /* System endpoints */
    @PUT
    @Path("sys/mod_logs")
    RestResponse<List<SysModLogData>> addModLog(SysModLogData newLogData);
}
