/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.apis;

import java.util.List;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.committerpaperwork.apis.models.MailingList;

/**
 * Bindings to interact with the Mailing list API.
 */
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "mailing-list")
public interface MailingListAPI {

    @GET
    @Path("mailing-list")
    List<MailingList> getProjectMailingLists(@QueryParam("project_id") String projectId);
}
