/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.apis;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.committerpaperwork.apis.models.SlimMemberOrganization;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.jboss.resteasy.reactive.RestResponse;

import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;

/**
 * @author martin
 *
 */
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "membership")
public interface MembershipAPI {

    @GET
    @Path("organizations/slim")
    RestResponse<List<SlimMemberOrganization>> getOrganizations(@BeanParam BaseAPIParameters params);

}
