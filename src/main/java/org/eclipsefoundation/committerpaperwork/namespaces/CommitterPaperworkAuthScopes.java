/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.namespaces;

/**
 * Lists the different authorization scopes used by the API.
 */
public class CommitterPaperworkAuthScopes {
    private static final String BASE_PERMISSION = "committer_paperwork_";
    public static final String READ = BASE_PERMISSION + "retrieve";
    public static final String WRITE = BASE_PERMISSION + "update";
    public static final String DELETE = BASE_PERMISSION + "delete";

    /**
     * Hide constructor as all fields are public and static, so not needed.
     */
    private CommitterPaperworkAuthScopes() {
    }
}
