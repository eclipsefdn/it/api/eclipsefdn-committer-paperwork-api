/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.namespaces;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * Represents the possible states for the committer paperwork submission.
 * 
 * @author Martin Lowe
 *
 */
public enum CommitterPaperworkStatus {
    EMPLOYER_INFORMATION(0), FORM_SUBMISSION(1), APPROVAL(2), COMPLETED(3), FAILED(4), PROVISIONED(5), ERROR(6),
    APPROVAL_FINAL(7), UNSTARTED(9);

    private int code;

    private CommitterPaperworkStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

    /**
     * Retrieve a status enum by its code if it exists
     * 
     * @param code the paperwork status code to retrieve the status for
     * @return the election status object if a matching code is found
     */
    public static Optional<CommitterPaperworkStatus> getStatusByCode(int code) {
        return Stream.of(values()).filter(cpes -> cpes.getCode() == code).findFirst();
    }
}
