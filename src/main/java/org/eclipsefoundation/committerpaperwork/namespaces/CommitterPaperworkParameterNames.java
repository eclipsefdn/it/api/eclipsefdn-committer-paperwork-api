/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.namespaces;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipsefoundation.utils.namespace.UrlParameterNamespace;

import jakarta.inject.Singleton;

/**
 * Parameter names used when reading query params into DB query parameters
 * 
 * @author Martin Lowe
 *
 */
@Singleton
public class CommitterPaperworkParameterNames implements UrlParameterNamespace {
    public static final String REQ_WHEN_NAME_RAW = "req_when";
    public static final String PERMISSION_OPERATION_NAME_RAW = "op";
    public static final String EMAIL_NAME_RAW = "email";
    public static final String TOKEN_NAME_RAW = "token";
    public static final String FORGE_NAME_RAW = "forge";
    public static final String PROJECT_ID_NAME_RAW = "project_id";
    public static final String USERNAME_NAME_RAW = "username";
    public static final String UID_NAME_RAW = "uid";
    public static final String ELECTION_NID_NAME_RAW = "election_nid";
    public static final String ELECTION_STATUS_NAME_RAW = "election_status";
    public static final String COMMITTER_PAPERWORK_NID_NAME_RAW = "committer_paperwork_nid";
    public static final String COMMITTER_PAPERWORK_STATUS_NAME_RAW = "committer_paperwork_status";
    public static final String COMMITTER_PAPERWORK_STATUS_EXCLUDED_NAME_RAW = "committer_paperwork_status_excluded";
    public static final String SPEC_PROJECT_WORKING_GROUP_NAME_RAW = "spec_project_working_group";
    public static final UrlParameter FORGE = new UrlParameter(FORGE_NAME_RAW);
    public static final UrlParameter PROJECT_ID = new UrlParameter(PROJECT_ID_NAME_RAW);
    public static final UrlParameter USERNAME = new UrlParameter(USERNAME_NAME_RAW);
    public static final UrlParameter UID = new UrlParameter(UID_NAME_RAW);
    public static final UrlParameter ELECTION_NID = new UrlParameter(ELECTION_NID_NAME_RAW);
    public static final UrlParameter ELECTION_STATUS = new UrlParameter(ELECTION_STATUS_NAME_RAW);
    public static final UrlParameter COMMITTER_PAPERWORK_NID = new UrlParameter(COMMITTER_PAPERWORK_NID_NAME_RAW);
    public static final UrlParameter COMMITTER_PAPERWORK_STATUS = new UrlParameter(COMMITTER_PAPERWORK_STATUS_NAME_RAW);
    public static final UrlParameter COMMITTER_PAPERWORK_STATUS_EXCLUDED = new UrlParameter(
            COMMITTER_PAPERWORK_STATUS_EXCLUDED_NAME_RAW);
    public static final UrlParameter SPEC_PROJECT_WORKING_GROUP = new UrlParameter(SPEC_PROJECT_WORKING_GROUP_NAME_RAW);

    // Unmodifiable list to use downstream
    public static final List<UrlParameter> PARAMETERS = Collections
            .unmodifiableList(Arrays
                    .asList(FORGE, PROJECT_ID, USERNAME, UID, ELECTION_NID, ELECTION_STATUS, COMMITTER_PAPERWORK_NID,
                            COMMITTER_PAPERWORK_STATUS, COMMITTER_PAPERWORK_STATUS_EXCLUDED,
                            SPEC_PROJECT_WORKING_GROUP));

    @Override
    public List<UrlParameter> getParameters() {
        return PARAMETERS;
    }

}
