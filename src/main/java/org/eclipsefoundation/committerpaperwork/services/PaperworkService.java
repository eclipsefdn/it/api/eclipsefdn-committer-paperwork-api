/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.services;

import org.eclipsefoundation.committerpaperwork.dtos.eclipseapi.CommitterPaperwork;
import org.eclipsefoundation.committerpaperwork.models.ProvisioningRequestData;
import org.eclipsefoundation.committerpaperwork.models.RetirementRequestData;
import org.eclipsefoundation.efservices.api.models.EfUser;

/**
 * Interface for some of the complex workflows related to committer paperwork.
 * 
 * @author Martin Lowe
 *
 */
public interface PaperworkService {

    /**
     * Retire a committer from a project
     *
     * To summarize:
     * <ol>
     * <li>Update the inactive column in the PeopleProject table to the current date to set the user as inactive for the
     * specified project
     * <li>Insert into sysmodlog about the update in the PeopleProject table
     * <li>Delete the user record in OrganizationContacts table
     * <li>Insert into sysmodlog about the deletion in the OrganizationContacts table
     * <li>Update the Provisioning column in the People table to state that the user has been successfully retired from
     * the project
     * <li>Insert a record in AccountRequest table
     * </ol>
     * 
     * @param request the current retirement request to act on
     * @param user the user that is being retired
     * @param requestorUser the user that requested the retirement.
     */
    void retireUser(RetirementRequestData request, String user, EfUser requestorUser);

    /**
     * Start the committer provisioning process
     *
     * To summarize:
     * <ol>
     * <li>Insert a record for the user into the People table
     * <li>If the individual works for a member company:
     * <li>Update the People record so that type='EM' and IsMember=1
     * <li>Delete any existing "EMPLY" records for the user from the OrganizationContacts table
     * <li>Insert a new "EMPLY record associating the company with the individual
     * <li>Update the People record to set the Provisioning message to "$projectid - $datestamp"
     * <li>If the individual had previously been a "CM" on the project, update the existing PeopleProject record
     * Otherwise, insert a new "CM" record on the PeopleProject.
     * </ol>
     * 
     * @param request the current provisioning request to act on
     * @param paperwork the paperwork associated with the current request
     * @param user the user that is being provisioned
     * @return the provisioning string result for the request
     */
    String provisionUser(ProvisioningRequestData request, CommitterPaperwork paperwork, EfUser user);
}
