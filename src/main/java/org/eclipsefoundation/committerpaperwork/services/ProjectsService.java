/*********************************************************************
* Copyright (c) 2020, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.committerpaperwork.services;

import java.util.Optional;

import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectData;

/**
 * Interface for retrieving Eclipse project data.
 * 
 * @author Martin Lowe
 *
 */
public interface ProjectsService {

    /**
     * Retrieve a single project, looking up the project based on the project ID, e.g. iot.sparkplug
     * 
     * @param id the full ID of the project to lookup
     * @return an optional containing the project data if it exists, otherwise empty.
     */
    Optional<ProjectData> getProject(String id);
}
