/*********************************************************************
* Copyright (c) 2020 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*	      Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.committerpaperwork.services.impl;

import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.committerpaperwork.apis.FoundationDBAPI;
import org.eclipsefoundation.committerpaperwork.services.ProjectsService;
import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;

/**
 * Using the already available FoundationDB binding, provides read access to projects in the FoundationDB.
 *
 * @author Martin Lowe
 */
@ApplicationScoped
public class DefaultProjectsService implements ProjectsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultProjectsService.class);

    @RestClient
    FoundationDBAPI fdbAPI;
    @Inject
    CachingService cache;

    @Override
    public Optional<ProjectData> getProject(String id) {
        LOGGER.trace("Fetching project with id '{}'", id);
        try {
            return cache.get(id, new MultivaluedHashMap<>(), ProjectData.class, () -> fdbAPI.getProjects(id)).data();
        } catch (RuntimeException e) {
            LOGGER.warn("Error fetching project with id '{}'", id, e);
            return Optional.empty();
        }
    }
}
