/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.services.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.committerpaperwork.apis.FoundationDBAPI;
import org.eclipsefoundation.committerpaperwork.apis.MailingListAPI;
import org.eclipsefoundation.committerpaperwork.apis.MembershipAPI;
import org.eclipsefoundation.committerpaperwork.apis.models.MailingList;
import org.eclipsefoundation.committerpaperwork.apis.models.SlimMemberOrganization;
import org.eclipsefoundation.committerpaperwork.daos.EclipsePersistenceDao;
import org.eclipsefoundation.committerpaperwork.dtos.eclipse.AccountRequests;
import org.eclipsefoundation.committerpaperwork.dtos.eclipse.AccountRequests.AccountRequestsCompositeId;
import org.eclipsefoundation.committerpaperwork.dtos.eclipseapi.CommitterPaperwork;
import org.eclipsefoundation.committerpaperwork.models.ProvisioningRequestData;
import org.eclipsefoundation.committerpaperwork.models.RetirementRequestData;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkParameterNames;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkStatus;
import org.eclipsefoundation.committerpaperwork.services.PaperworkService;
import org.eclipsefoundation.core.service.APIMiddleware;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.foundationdb.client.runtime.model.organization.OrganizationContactData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleProjectData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogData;
import org.eclipsefoundation.http.helper.IPParser;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.eclipsefoundation.utils.helper.TransformationHelper;
import org.jboss.resteasy.reactive.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * Default service implementation for committer paperwork workflows.
 * 
 * @author Martin Lowe
 *
 */
@Singleton
public class DefaultPaperworkService implements PaperworkService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPaperworkService.class);

    public static final String GROUP_REMOVAL_VALUE = "GROUPREMOVE";
    public static final String GROUP_ADD_VALUE = "GROUPADD";

    @Inject
    DefaultHibernateDao dao;
    @Inject
    EclipsePersistenceDao eclipseDao;
    @Inject
    FilterService filters;
    @Inject
    APIMiddleware middleware;
    @Inject
    CachingService cache;

    @RestClient
    FoundationDBAPI fdbAPI;
    @RestClient
    MembershipAPI membership;
    @RestClient
    MailingListAPI mailingListAPI;

    @Inject
    RequestWrapper wrap;
    @Inject
    IPParser ipHelper;

    @Override
    public void retireUser(RetirementRequestData request, String username, EfUser requestorUser) {
        // use sanitized value to ensure no log mocking/abuse happens
        String sanitizedUsername = TransformationHelper.formatLog(username);
        // check that the user hasn't already been processed for retirement
        if (request.getInfo().getProjectRole().equalsIgnoreCase("CM")) {
            checkForExistingGroupPermissionsRequest(request.getInfo().getProjectId(), sanitizedUsername, GROUP_REMOVAL_VALUE);
        }

        // validate retired user is PL/CM on given project
        Optional<List<PeopleProjectData>> userPeopleProjects = cache
                .get(sanitizedUsername, new MultivaluedHashMap<>(), PeopleProjectData.class,
                        () -> middleware.getAll(i -> fdbAPI.getPeopleProjects(i, sanitizedUsername)))
                .data();
        PeopleProjectData projectRelation = validateUserProjectPermissions(userPeopleProjects, sanitizedUsername, request.getInfo().getProjectId(),
                request.getInfo().getProjectRole());

        // retire the user from the project and log the action
        retireUserFromProject(sanitizedUsername, request, projectRelation);

        // remove user access from fdbAPI projects table and add request for account update if needed
        updateUserPermissionsForRetirement(request, sanitizedUsername, userPeopleProjects.get());

        // update the provisioning for the user
        updateProvisioningForRetirement(sanitizedUsername, request, requestorUser, projectRelation);

        // remove cached project data for the user, as it will have been updated
        cache.fuzzyRemove(sanitizedUsername, PeopleProjectData.class);
    }

    @Override
    public String provisionUser(ProvisioningRequestData request, CommitterPaperwork paperwork, EfUser user) {
        // check there isn't a pending provisioning request for the user
        checkForExistingGroupPermissionsRequest(paperwork.getProjectId(), user.name(), GROUP_ADD_VALUE);

        // check if user is flagged as part of a member organization
        boolean isMember = isUserInMemberOrg(user);
        LOGGER.debug("Is user {} in a member org? {}", user.name(), isMember);

        String username = user.name();
        // check if the person exists in FDB yet, if not create it
        PeopleData pd = fdbAPI.getPersonalRecord(username);
        LOGGER.debug("Found fdbAPI for user {}: {}", user, pd);
        if (pd == null) {
            pd = new PeopleData(username, user.firstName(), user.lastName(), isMember ? "EM" : "XX", isMember, user.mail(), "", "",
                    "", "", "", false, false, null, null, null, null, null, null);
            fdbAPI.updatePersonalRecord(pd);
        }

        // update the users OrganizationContacts to make sure they are current and accurate
        if (isMember) {
            updateUserContactsForProvisioning(username, paperwork, request);
        } else {
            LOGGER.debug("User '{}' is not part of a member org, organization contact updates will be skipped.", username);
        }

        // update the PeopleProject relations to include or reactivate entry for target user
        updateProjectPermissionsForProvisioning(username, paperwork);
        // update the user to indicate the provisioning action that took place
        String provisioningResponse = updateUserPostProvisioning(pd, paperwork, isMember);

        // track the account change request
        logAccountPermissionsChangeRequest(user.name(), paperwork.getProjectId(), GROUP_ADD_VALUE);
        subscribeToProjectMailingLists(user, paperwork);

        // update the paperwork to indicate that the provisioning has been completed
        updatePaperworkForProvisioning(paperwork);
        return provisioningResponse;
    }

    /**
     * Clean up the users state, removing employee relations and ensure that it reflects the current organization only.
     * 
     * @param username the user that is being provisioned
     * @param paperwork the committer paperwork for the provisioning request
     * @param request the actual provisioning request data.
     */
    private void updateUserContactsForProvisioning(String username, CommitterPaperwork paperwork, ProvisioningRequestData request) {
        LOGGER.trace("Starting user provisioning update for user {} in project {}", username, paperwork.getProjectId());
        // wrap in try-catch as this can error if the user is in a bad state w/o an org contact
        List<OrganizationContactData> contacts = Collections.emptyList();
        try {
            // remove other OrgContact entities
            contacts = middleware.getAll(i -> fdbAPI.getOrganzationContacts(i, username, "EMPLY"));
            contacts
                    .stream()
                    .filter(c -> c.organizationID() != request.getOrganization().getId())
                    .forEach(contact -> fdbAPI.removeOrganizationContact(contact.organizationID(), contact.personID(), contact.relation()));
        } catch (WebApplicationException e) {
            // do not error out, as there are some edge cases where this can happen. Our update call should correct the
            // state
            LOGGER.warn("Error while removing previous org contacts for {} in provisioning, will attempt to correct", username, e);
        }
        // check if we need to run the update before running
        if (contacts.stream().anyMatch(c -> c.organizationID() == request.getOrganization().getId())) {
            LOGGER
                    .info("User {} already an employee of organisation {}, no update needed for user contacts", username,
                            request.getOrganization().getId());
            return;
        }
        // add new organization contact for current request
        OrganizationContactData contact = fdbAPI
                .updateOrganizationContact(request.getOrganization().getId(),
                        new OrganizationContactData(request.getOrganization().getId(), username, "EMPLY", "", "", ""));
        if (contact == null) {
            throw new ServerErrorException(String
                    .format("Error while updating organization contact for user %s while provisioning for project %s", username,
                            paperwork.getProjectId()),
                    Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the PeopleProject table to remove CC role if its the only project the user has on record, and if the role that is being
     * retired is committer permissions, initiate a request to have those permissions revoked in the Account request table.
     * 
     * @param request the retirement request that is being acted on
     * @param efUser user account object for account being updated
     * @param userPeopleProjects the list of fdbAPI projects for the user to check for other projects
     */
    private void updateUserPermissionsForRetirement(RetirementRequestData request, String username,
            List<PeopleProjectData> userPeopleProjects) {
        // remove org contact committer role (CC) if current user has no other projects
        if (userPeopleProjects
                .stream()
                .filter(input -> !input.projectID().equalsIgnoreCase(request.getInfo().getProjectId()) && input.inactiveDate() == null)
                .count() == 0) {
            LOGGER.info("User '{}' has no other associated projects, removing CC role", username);
            // iterate over the CC role contact entries and submit them for removal
            try {
                middleware
                        .getAll(i -> fdbAPI.getOrganzationContacts(i, username, "CC"))
                        .stream()
                        .forEach(org -> fdbAPI.removeOrganizationContact(org.organizationID(), org.personID(), org.relation()));
            } catch (WebApplicationException e) {
                // if the contacts are missing, that is an acceptable case
                if (e.getResponse().getStatus() == Status.NOT_FOUND.getStatusCode()) {
                    LOGGER.info("No contacts found for {}, skipping removal of contact relations", username);
                } else {
                    // rethrow if is not a missing entity
                    throw e;
                }
            }
        }

        // if committer, request removal perms in account_requests table (eclipse db)
        if (request.getInfo().getProjectRole().equalsIgnoreCase("CM")) {
            logAccountPermissionsChangeRequest(username, request.getInfo().getProjectId(), GROUP_REMOVAL_VALUE);
        }
    }

    /**
     * Updates the fdbAPI project table to set an inactive date for the given project to effectively retire the user in the given role.
     * Additionally, an entry in the modification log will be created to track the actioned request.
     * 
     * @param username the user that is retiring from a role
     * @param request the retirement request, containing project and role to act on
     * @param projectRelation the project relation that is being updated.
     */
    private void retireUserFromProject(String username, RetirementRequestData request, PeopleProjectData projectRelation) {
        // Actually retire user from fdbAPI project
        RestResponse<PeopleProjectData> retirementResponse = fdbAPI
                .updatePeopleProjectEntry(username, new PeopleProjectData(projectRelation.projectID(), projectRelation.personID(),
                        projectRelation.relation(), projectRelation.activeDate(), new Date(), projectRelation.editBugs()));
        if (retirementResponse.getStatus() != Response.Status.OK.getStatusCode()) {
            LOGGER
                    .error("Error while retiring user '{}' for project '{}' with role '{}'", username, projectRelation.projectID(),
                            projectRelation.relation());
            throw new ServerErrorException("Error while retiring user for current request", Response.Status.INTERNAL_SERVER_ERROR);
        }

        // do sys mod log update
        RestResponse<List<SysModLogData>> modLogActionResponse = fdbAPI
                .addModLog(new SysModLogData(null, "OrganizationContacts", username,
                        "Committer Retirement - Deleted Committer Member relation", "DELETED By " + request.getInfo().getRequestor(),
                        request.getInfo().getRequestor(), DateTimeHelper.now()));
        // log failures, but we shouldn't short circuit the updates if we can't add a mod log
        if (modLogActionResponse.getStatus() != Response.Status.OK.getStatusCode()) {
            LOGGER
                    .error("Error while adding modlog for retirement of user '{}' for project '{}' with role '{}'", username,
                            projectRelation.projectID(), projectRelation.relation());
        }
    }

    private String updateUserPostProvisioning(PeopleData basePersonData, CommitterPaperwork paperwork, boolean isMember) {
        // update the personnel record to add new provisioning and to update member type if required
        String provisioningValue = "Provisioning:\r\n" + paperwork.getProjectId() + " - "
                + DateTimeHelper.toRFC3339(new Date(System.currentTimeMillis()));
        fdbAPI
                .updatePersonalRecord(
                        new PeopleData(basePersonData.personID(), basePersonData.fname(), basePersonData.lname(), isMember ? "EM" : "XX",
                                isMember, basePersonData.email(), basePersonData.phone(), basePersonData.fax(), basePersonData.mobile(),
                                basePersonData.comments(),
                                (StringUtils.isNotBlank(basePersonData.provisioning()) ? basePersonData.provisioning() : "")
                                        + provisioningValue,
                                basePersonData.unixAcctCreated(), basePersonData.issuesPending(), basePersonData.memberPassword(),
                                basePersonData.memberSince(), basePersonData.latitude(), basePersonData.longitude(), basePersonData.blog(),
                                basePersonData.scrmGuid()));
        // do sys mod log update
        RestResponse<List<SysModLogData>> modLogActionResponse = fdbAPI
                .addModLog(new SysModLogData(null, "Person", "portal", basePersonData.personID(), "DB_MERGE", "api-paperwork",
                        DateTimeHelper.now()));
        // log failures, but we shouldn't short circuit the updates if we can't add a mod log
        if (modLogActionResponse.getStatus() != Response.Status.OK.getStatusCode()) {
            LOGGER
                    .error("Error while adding modlog for provisioning of user '{}' for project '{}' with role 'CM'",
                            basePersonData.personID(), paperwork.getProjectId());
        }
        return provisioningValue;
    }

    /**
     * Updates the users project permissions/roles through entries in the PeopleProject table, either creating or reactivating a previous
     * entry and granting the CM committer privilege.
     * 
     * @param username the user being provisioned
     * @param paperwork the committer paperwork for the provisioning
     */
    private void updateProjectPermissionsForProvisioning(String username, CommitterPaperwork paperwork) {
        LOGGER.trace("Starting permission provisioning update for user {} in project {}", username, paperwork.getProjectId());
        // check if the user was previously a committer on the project
        List<PeopleProjectData> fdbAPIProjects = Collections.emptyList();
        try {
            fdbAPIProjects = middleware
                    .getAll(i -> fdbAPI.getPeopleProjects(i, username))
                    .stream()
                    .filter(ppd -> ppd.projectID().equalsIgnoreCase(paperwork.getProjectId()) && ppd.relation().equalsIgnoreCase("CM"))
                    .toList();
        } catch (WebApplicationException e) {
            // 404 isn't an error in this case, so just log that it happened and continue
            if (e.getResponse().getStatus() == Response.Status.NOT_FOUND.getStatusCode()) {
                LOGGER.trace("No people project entries found for paperwork w/ ID {}, will create missing entry", paperwork.getId());
            } else {
                LOGGER
                        .warn("Error while fetching people projects for paperwork with id {}, will attempt to create anyways",
                                paperwork.getId(), e);
            }
        }
        PeopleProjectData ppd;
        if (fdbAPIProjects.isEmpty()) {
            // Create new fdbAPI project entry and persist
            ppd = new PeopleProjectData(paperwork.getProjectId(), username, "CM", new Date(), null, false);
        } else {
            // update the existing record to reactivate the entry
            PeopleProjectData original = fdbAPIProjects.get(0);
            ppd = new PeopleProjectData(original.projectID(), original.personID(), original.relation(), new Date(), null,
                    original.editBugs());
        }

        // push the updated fdbAPI project entry
        RestResponse<PeopleProjectData> r = fdbAPI.updatePeopleProjectEntry(username, ppd);
        // check that the people project entry was successfully updated
        if (r.getStatus() != Status.OK.getStatusCode()) {
            throw new ServerErrorException(
                    String.format("Could not provision project permissions for paperwork with ID '%d'", paperwork.getId()),
                    Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update the provisioning field for the user that is retiring to indicate the following points:
     * 
     * <ul>
     * <li>Who initiated the request
     * <li>What project this affects
     * <li>When this action took effect
     * </ul>
     * 
     * @param username the user that is retiring from a role
     * @param request the retirement request, containing project and role to act on
     * @param requestorUser the user requesting the retirement.
     * @param projectRelation the project relation that is being updated.
     */
    private void updateProvisioningForRetirement(String username, RetirementRequestData request, EfUser requestorUser,
            PeopleProjectData projectRelation) {
        LOGGER.trace("Updating user {} provisioning for retirement from {}", username, projectRelation.projectID());
        // Update person record provisioning to indicate who retired the permissions
        PeopleData pd = fdbAPI.getPersonalRecord(username);
        RestResponse<List<PeopleData>> provisioningUpdate = fdbAPI
                .updatePersonalRecord(new PeopleData(pd.personID(), pd.fname(), pd.lname(), pd.type(), pd.member(), pd.email(), pd.phone(),
                        pd.fax(), pd.mobile(), pd.comments(),
                        new StringBuilder(StringUtils.isNotBlank(pd.provisioning()) ? pd.provisioning() : "")
                                .append("\r\n\r\n")
                                .append(request.getInfo().getProjectId())
                                .append(" - expired by ")
                                .append(requestorUser.firstName() + " " + requestorUser.lastName())
                                .append(" on ")
                                .append(DateTimeHelper.now().toString())
                                .toString(),
                        pd.unixAcctCreated(), pd.issuesPending(), pd.memberPassword(), pd.memberSince(), pd.latitude(), pd.longitude(),
                        pd.blog(), pd.scrmGuid()));
        // log failures, but we shouldn't short circuit the updates if we can't update provisioning
        if (provisioningUpdate.getStatus() != Response.Status.OK.getStatusCode()) {
            LOGGER
                    .error("Error while updating provisioning for retirement of user '{}' for project '{}' with role '{}'", username,
                            projectRelation.projectID(), projectRelation.relation());
        }
    }

    /**
     * Check user permissions for the given project to ensure that the user has the permission to be retired before processing.
     * 
     * @param userPeopleProjects list of projects to filter
     * @param username the user name for retiring user
     * @param projectId the full ID of the project, e.g. iot.sparkplug
     * @param targetRole the code of the role to be retired
     * @return the user project relation for further processing
     * @throws BadRequestException if the user does not have the given permission active on the project
     */
    private PeopleProjectData validateUserProjectPermissions(Optional<List<PeopleProjectData>> userPeopleProjects, String username,
            String projectId, String targetRole) throws BadRequestException {
        // check that the user has any permissions before filtering
        if (userPeopleProjects.isEmpty() || userPeopleProjects.get().isEmpty()) {
            throw new BadRequestException("User %s does not have elevated rights on any project");
        }

        // retrieve the current project relation for the given user, filtering on project, role, and active state
        Optional<PeopleProjectData> projectRelation = userPeopleProjects
                .get()
                .stream()
                .filter(input -> input.projectID().equalsIgnoreCase(projectId) && input.relation().equalsIgnoreCase(targetRole)
                        && input.inactiveDate() == null)
                .findFirst();
        if (projectRelation.isEmpty()) {
            throw new BadRequestException(String.format("User %s does not have role %s on project %s", username, targetRole, projectId));
        }
        return projectRelation.get();
    }

    /**
     * Retrieves member fdbAPI and checks if the user is in a member organization.
     * 
     * @param user the user to check for member org employment.
     * @return true if the user can be mapped to a member organization, false otherwise
     */
    private boolean isUserInMemberOrg(EfUser user) {
        if (user.orgId() == null) {
            return false;
        }
        // retrieve cached member orgs and check if the target user is part of a member org
        Optional<List<SlimMemberOrganization>> orgs = cache
                .get("all", new MultivaluedHashMap<>(), SlimMemberOrganization.class,
                        () -> middleware.getAll(i -> membership.getOrganizations(i)))
                .data();
        if (orgs.isEmpty()) {
            LOGGER.warn("Unable to detect member fdbAPI, provisioning may be inaccurate for type and org membership.");
            return false;
        }
        return orgs.get().stream().anyMatch(o -> user.orgId() != null && o.getOrganizationID() == user.orgId());
    }

    /**
     * Logs a request for an accounts permissions update in the AccountRequests table associated with the given user and project, with the
     * operation to perform set into the first/last name fields.
     * 
     * @param user the Eclipse user that needs updated permissions
     * @param projectId the project to update user permissions for
     * @param requestType the operation type to perform
     */
    private void logAccountPermissionsChangeRequest(String username, String projectId, String requestType) {
        AccountRequestsCompositeId arci = new AccountRequestsCompositeId();
        arci.setEmail(username);
        arci.setReqWhen(DateTimeHelper.now());
        AccountRequests ar = new AccountRequests();
        ar.setId(arci);
        ar.setFname(requestType);
        ar.setLname(requestType);
        ar.setPassword("");
        ar.setIp(ipHelper.getBestMatchingIP());
        ar.setToken(projectId);
        ar.setNewEmail(requestType);
        eclipseDao.add(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class)), Arrays.asList(ar));
    }

    /**
     * Create an account request record to add an email subscription for a user. This will need to be persisted separately.
     * 
     * @param user the user to request a subscription for
     * @param mailingListEmail mailing list email address.
     * @return the request for the subscription update.
     */
    private AccountRequests logAccountSubscriptionRequest(EfUser user, String mailingListEmail) {
        AccountRequestsCompositeId arci = new AccountRequestsCompositeId();
        arci.setEmail(user.mail());
        arci.setReqWhen(DateTimeHelper.now());
        AccountRequests ar = new AccountRequests();
        ar.setId(arci);
        ar.setFname("SUBSCRIBE");
        ar.setLname("SUBSCRIBE");
        ar.setPassword("");
        ar.setIp(ipHelper.getBestMatchingIP());
        ar.setToken("");
        ar.setNewEmail(mailingListEmail);
        return ar;
    }

    /**
     * Checks the account request table that contains requests for access updates for in-progress retirement requests for the given user and
     * project.
     * 
     * @param request the retirement request, containing project and role to act on
     * @param efUser the user object for the user that is being retired
     */
    private void checkForExistingGroupPermissionsRequest(String projectId, String efUser, String operation) {
        // Check if removal already requested in account_requests table (eclipse db)
        MultivaluedMap<String, String> arParams = new MultivaluedHashMap<>();
        arParams.add(CommitterPaperworkParameterNames.USERNAME_NAME_RAW, efUser);
        arParams.add(CommitterPaperworkParameterNames.TOKEN_NAME_RAW, projectId);
        arParams.add(CommitterPaperworkParameterNames.PERMISSION_OPERATION_NAME_RAW, operation);
        // count the number of requests for the user given email and project ID.
        long count = eclipseDao.count(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class), arParams));
        // if there are already any entries, we should not process.
        if (count > 0) {
            LOGGER.warn("Duplicate request recieved to modify '{}' permissions ({}) on project '{}'", efUser, operation, projectId);
            throw new WebApplicationException("The request already exist for this user and project.", Response.Status.CONFLICT);
        }
    }

    /**
     * Updates the paperwork entry to change the status from completed to provisioned.
     * 
     * @param paperwork the committer paperwork entry to finish provisioning
     */
    private void updatePaperworkForProvisioning(CommitterPaperwork paperwork) {
        // update the status to provisioned and touch the changed timestamp
        paperwork.setCommitterPaperworkStatus(CommitterPaperworkStatus.PROVISIONED.getCode());
        paperwork.setChanged(TimeUnit.SECONDS.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS));

        List<CommitterPaperwork> updatedPaperwork = dao
                .add(new RDBMSQuery<>(wrap, filters.get(CommitterPaperwork.class)), Arrays.asList(paperwork));
        if (updatedPaperwork == null || updatedPaperwork.isEmpty()) {
            throw new ServerErrorException(
                    String.format("Could not update the status of paperwork for user with UID '%s' to provisioned", paperwork.getUid()),
                    Status.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Create entries in the account requests table to request subscriptions to mailing lists associated with their paperwork.
     * 
     * @param user the user associated with the paperwork
     * @param paperwork the committer paperwork entry associated with the current request
     */
    private void subscribeToProjectMailingLists(EfUser user, CommitterPaperwork paperwork) {
        String projectId = paperwork.getProjectId().toLowerCase();
        // get the mailing lists associated with the given project
        Optional<List<MailingList>> mailingLists = cache
                .get(paperwork.getProjectId().toLowerCase(), null, MailingList.class,
                        () -> mailingListAPI.getProjectMailingLists(projectId))
                .data();
        if (mailingLists.isEmpty()) {
            LOGGER.error("Could not fetch mailing lists associated with project {}", projectId);
            return;
        } else if (mailingLists.get().isEmpty()) {
            LOGGER
                    .info("No mailing lists could be found associated with project {} for paperwork with ID {}, no subscriptions will be requested",
                            projectId, paperwork.getId());
            return;
        }

        // iterate through the project mailing lists and create subscription requests
        List<AccountRequests> requests = mailingLists
                .get()
                .stream()
                .map(mailingList -> logAccountSubscriptionRequest(user, mailingList.getEmail()))
                .toList();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Adding {} subscriptions for user {} for project {}", requests.size(), user.name(), paperwork.getProjectId());
        }

        // persist the requests, and do a check to see that all of the records were properly persisted
        List<AccountRequests> updatedRequests = eclipseDao.add(new RDBMSQuery<>(wrap, filters.get(AccountRequests.class)), requests);
        if (updatedRequests.size() != requests.size()) {
            LOGGER
                    .warn("Could not update all mailing list subscriptions for user {} in project {}", user.name(),
                            paperwork.getProjectId());
        }
    }
}
