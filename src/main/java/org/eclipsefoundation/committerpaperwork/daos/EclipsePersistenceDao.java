/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.daos;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.EntityManager;

import org.eclipsefoundation.persistence.dao.impl.BaseHibernateDao;

import io.quarkus.hibernate.orm.PersistenceUnit;

/**
 * Persistence DAO for the 'eclipse' datasource.
 * 
 * @author Martin Lowe
 *
 */
@Singleton
public class EclipsePersistenceDao extends BaseHibernateDao {

    @PersistenceUnit("eclipse")
    @Inject
    EntityManager em;

    @Override
    protected EntityManager getPrimaryEntityManager() {
        return em;
    }
}
