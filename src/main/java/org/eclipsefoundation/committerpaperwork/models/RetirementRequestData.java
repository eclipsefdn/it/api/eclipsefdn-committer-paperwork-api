/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_RetirementRequestData.Builder.class)
public abstract class RetirementRequestData {

    public abstract Parameters getInfo();

    public static Builder builder() {
        return new AutoValue_RetirementRequestData.Builder();
    }

    public abstract Builder toBuilder();

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setInfo(Parameters info);
        public abstract RetirementRequestData build();
    }

    @AutoValue
    @JsonDeserialize(builder = AutoValue_RetirementRequestData_Parameters.Builder.class)
    public abstract static class Parameters {
        public abstract String getRequestor();
        public abstract String getProjectId();
        public abstract String getProjectRole();

        public static Builder builder() {
            return new AutoValue_RetirementRequestData_Parameters.Builder();
        }

        public abstract Builder toBuilder();

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setRequestor(String requestor);
            public abstract Builder setProjectId(String projectId);
            public abstract Builder setProjectRole(String projectRole);
            public abstract Parameters build();
        }
    }


}
