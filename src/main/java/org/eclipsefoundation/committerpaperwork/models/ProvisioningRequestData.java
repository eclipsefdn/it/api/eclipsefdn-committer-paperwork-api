/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_ProvisioningRequestData.Builder.class)
public abstract class ProvisioningRequestData {

    public abstract Parameters getOrganization();

    public static Builder builder() {
        return new AutoValue_ProvisioningRequestData.Builder();
    }

    public abstract Builder toBuilder();

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setOrganization(Parameters organization);
        public abstract ProvisioningRequestData build();
    }
    @AutoValue
    @JsonDeserialize(builder = AutoValue_ProvisioningRequestData_Parameters.Builder.class)
    public abstract static class Parameters {
        public abstract int getId();

        public static Builder builder() {
            return new AutoValue_ProvisioningRequestData_Parameters.Builder();
        }

        public abstract Builder toBuilder();

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setId(int id);
            public abstract Parameters build();
        }
    }


}
