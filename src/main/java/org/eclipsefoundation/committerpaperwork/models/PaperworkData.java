/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.models;

import jakarta.annotation.Nullable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_PaperworkData.Builder.class)
public abstract class PaperworkData {

    public abstract int getId();
    public abstract String getForge();
    public abstract String getProjectId();
    public abstract String getName();
    @Nullable
    public abstract Integer getElectionNid();
    public abstract int getElectionStatus();
    @Nullable
    public abstract Integer getCommitterPaperworkNid();
    public abstract int getCommitterPaperworkStatus();
    public abstract String getEtag();
    public abstract long getCreated();
    public abstract long getChanged();
    public abstract int getSpecProjectWorkingGroupTid();

    public static Builder builder() {
        return new AutoValue_PaperworkData.Builder();
    }

    public abstract Builder toBuilder();

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setId(int id);
        public abstract Builder setForge(String forge);
        public abstract Builder setProjectId(String projectId);
        public abstract Builder setName(String name);
        public abstract Builder setElectionNid(@Nullable Integer electionNid);
        public abstract Builder setElectionStatus(int electionStatus);
        public abstract Builder setCommitterPaperworkNid(@Nullable Integer committerPaperworkNid);
        public abstract Builder setCommitterPaperworkStatus(int committerPaperworkStatus);
        public abstract Builder setEtag(String etag);
        public abstract Builder setCreated(long created);
        public abstract Builder setChanged(long changed);
        public abstract Builder setSpecProjectWorkingGroupTid(int specProjectWorkingGroupTid);
        public abstract PaperworkData build();
    }

}
