/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_ProspectivePaperworkData.Builder.class)
public abstract class ProspectivePaperworkData {

    public abstract Parameters getParameters();

    public static Builder builder() {
        return new AutoValue_ProspectivePaperworkData.Builder();
    }

    public abstract Builder toBuilder();

    @AutoValue.Builder
    @JsonPOJOBuilder(withPrefix = "set")
    public abstract static class Builder {
        public abstract Builder setParameters(Parameters forge);
        public abstract ProspectivePaperworkData build();
    }
    @AutoValue
    @JsonDeserialize(builder = AutoValue_ProspectivePaperworkData_Parameters.Builder.class)
    public abstract static class Parameters {
        public abstract String getForge();
        public abstract String getProjectId();
        public abstract int getElectionNid();
        public abstract int getElectionStatus();

        public static Builder builder() {
            return new AutoValue_ProspectivePaperworkData_Parameters.Builder();
        }

        public abstract Builder toBuilder();

        @AutoValue.Builder
        @JsonPOJOBuilder(withPrefix = "set")
        public abstract static class Builder {
            public abstract Builder setForge(String forge);
            public abstract Builder setProjectId(String projectId);
            public abstract Builder setElectionNid(int electionNid);
            public abstract Builder setElectionStatus(int electionStatus);
            public abstract Parameters build();
        }
    }


}
