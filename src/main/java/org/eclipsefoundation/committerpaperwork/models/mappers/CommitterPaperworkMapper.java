/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.models.mappers;

import java.util.Optional;

import org.eclipsefoundation.committerpaperwork.dtos.eclipseapi.CommitterPaperwork;
import org.eclipsefoundation.committerpaperwork.models.PaperworkData;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.api.models.UserSearchParams;
import org.eclipsefoundation.efservices.services.ProfileService;
import org.eclipsefoundation.persistence.config.QuarkusMappingConfig;
import org.eclipsefoundation.persistence.dao.PersistenceDao;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import io.quarkus.arc.Arc;

/**
 * @author Martin Lowe
 *
 */
@Mapper(config = QuarkusMappingConfig.class)
public interface CommitterPaperworkMapper extends BaseEntityMapper<CommitterPaperwork, PaperworkData> {

    @Mapping(source = "uid", target = "name", qualifiedByName = "uidToName")
    PaperworkData toModel(CommitterPaperwork dtoEntity);

    @Mapping(source = "name", target = "uid", qualifiedByName = "nameToUid")
    CommitterPaperwork toDTO(PaperworkData model, @Context PersistenceDao repo);

    @Named("uidToName")
    default String mapUidToUsername(String uid) {
        ProfileService dus = Arc.container().instance(ProfileService.class).get();
        Optional<EfUser> u = dus.performUserSearch(new UserSearchParams(uid, null, null));
        return u.isPresent() ? u.get().name() : "";
    }

    @Named("nameToUid")
    default String mapUsernameToUid(String username) {
        ProfileService dus = Arc.container().instance(ProfileService.class).get();
        Optional<EfUser> u = dus.fetchUserByUsername(username, false);
        return u.isPresent() ? u.get().uid() : "";
    }
}
