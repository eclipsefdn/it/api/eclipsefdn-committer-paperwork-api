/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.helpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.Hex;
import org.eclipsefoundation.http.exception.ApplicationException;

/**
 * Helper to centralize generation of etag values.
 * 
 * @author Martin Lowe
 *
 */
public final class ETagHelper {

    /**
     * Hash content using SHA-256 and convert output to a string to be used as an etag.
     * 
     * @param content non empty string to hash
     * @return the hashed etag for the content
     */
    public static String generateEtag(String content) {
        // we cannot hash empty content
        if (StringUtils.isBlank(content)) {
            throw new IllegalArgumentException("Content to hash to etag should not be empty");
        }
        try {
            // generate a message digest and hash the content
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hash = md.digest(content.getBytes());
            // convert the byte digest back into a hexadecimal string
            return new String(Hex.encode(hash));
        } catch (NoSuchAlgorithmException e) {
            throw new ApplicationException(String.format("Error generating ETag from content: %s", content), e);
        }
    }

    private ETagHelper() {
    }
}
