/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.dtos.eclipseapi;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.committerpaperwork.helpers.ETagHelper;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkParameterNames;
import org.eclipsefoundation.efservices.services.ProfileService;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * DTO representing the committer paperwork table in the Eclipse API DB.
 * 
 * @author Martin Lowe
 *
 */
@Entity
@Table(name = "committer_paperwork")
public class CommitterPaperwork extends BareNode {
    public static final DtoTable TABLE = new DtoTable(CommitterPaperwork.class, "eacp");

    private static final int DEFAULT_STRING_BUILDER_SIZE = 256;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String forge;
    private String projectId;
    private String uid;
    private Integer electionNid;
    private int electionStatus;
    private Integer committerPaperworkNid;
    private int committerPaperworkStatus;
    private String etag;
    private long created;
    private long changed;
    private long specProjectWorkingGroupTid;

    /**
     * @return the id
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the forge
     */
    public String getForge() {
        return forge;
    }

    /**
     * @param forge the forge to set
     */
    public void setForge(String forge) {
        this.forge = forge;
    }

    /**
     * @return the projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * @param projectId the projectId to set
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return the electionNid
     */
    public Integer getElectionNid() {
        return electionNid;
    }

    /**
     * @param electionNid the electionNid to set
     */
    public void setElectionNid(Integer electionNid) {
        this.electionNid = electionNid;
    }

    /**
     * @return the electionStatus
     */
    public int getElectionStatus() {
        return electionStatus;
    }

    /**
     * @param electionStatus the electionStatus to set
     */
    public void setElectionStatus(int electionStatus) {
        this.electionStatus = electionStatus;
    }

    /**
     * @return the committerPaperworkNid
     */
    public Integer getCommitterPaperworkNid() {
        return committerPaperworkNid;
    }

    /**
     * @param committerPaperworkNid the committerPaperworkNid to set
     */
    public void setCommitterPaperworkNid(Integer committerPaperworkNid) {
        this.committerPaperworkNid = committerPaperworkNid;
    }

    /**
     * @return the committerPaperworkStatus
     */
    public int getCommitterPaperworkStatus() {
        return committerPaperworkStatus;
    }

    /**
     * @param committerPaperworkStatus the committerPaperworkStatus to set
     */
    public void setCommitterPaperworkStatus(int committerPaperworkStatus) {
        this.committerPaperworkStatus = committerPaperworkStatus;
    }

    /**
     * @return the eTag
     */
    public String getEtag() {
        return etag;
    }

    /**
     * @param eTag the eTag to set
     */
    public void setEtag(String eTag) {
        this.etag = eTag;
    }

    /**
     * @return the created
     */
    public long getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(long created) {
        this.created = created;
    }

    /**
     * @return the changed
     */
    public long getChanged() {
        return changed;
    }

    /**
     * @param changed the changed to set
     */
    public void setChanged(long changed) {
        this.changed = changed;
    }

    /**
     * @return the specProjectWorkingGroupTid
     */
    public long getSpecProjectWorkingGroupTid() {
        return specProjectWorkingGroupTid;
    }

    /**
     * @param specProjectWorkingGroupTid the specProjectWorkingGroupTid to set
     */
    public void setSpecProjectWorkingGroupTid(long specProjectWorkingGroupTid) {
        this.specProjectWorkingGroupTid = specProjectWorkingGroupTid;
    }

    public String generateEtag() {
        StringBuilder sb = new StringBuilder(DEFAULT_STRING_BUILDER_SIZE);
        sb.append(getChanged());
        sb.append(getCommitterPaperworkStatus());
        sb.append(getCreated());
        sb.append(getElectionStatus());
        sb.append(getSpecProjectWorkingGroupTid());
        sb.append(getCommitterPaperworkNid());
        sb.append(getElectionNid());
        sb.append(getElectionStatus());
        sb.append(getForge());
        sb.append(getId());
        sb.append(getProjectId());
        sb.append(getUid());
        return ETagHelper.generateEtag(sb.toString());
    }

    @Override
    public int hashCode() {
        return Objects
                .hash(changed, committerPaperworkNid, committerPaperworkStatus, created, etag, electionNid,
                        electionStatus, forge, id, projectId, specProjectWorkingGroupTid, uid);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CommitterPaperwork other = (CommitterPaperwork) obj;
        return changed == other.changed && Objects.equals(committerPaperworkNid, other.committerPaperworkNid)
                && committerPaperworkStatus == other.committerPaperworkStatus && created == other.created
                && Objects.equals(etag, other.etag) && Objects.equals(electionNid, other.electionNid)
                && electionStatus == other.electionStatus && Objects.equals(forge, other.forge) && id == other.id
                && Objects.equals(projectId, other.projectId)
                && specProjectWorkingGroupTid == other.specProjectWorkingGroupTid && Objects.equals(uid, other.uid);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(DEFAULT_STRING_BUILDER_SIZE);
        builder.append("CommitterPaperwork [id=");
        builder.append(id);
        builder.append(", forge=");
        builder.append(forge);
        builder.append(", projectId=");
        builder.append(projectId);
        builder.append(", uid=");
        builder.append(uid);
        builder.append(", electionNid=");
        builder.append(electionNid);
        builder.append(", electionStatus=");
        builder.append(electionStatus);
        builder.append(", committerPaperworkNid=");
        builder.append(committerPaperworkNid);
        builder.append(", committerPaperworkStatus=");
        builder.append(committerPaperworkStatus);
        builder.append(", eTag=");
        builder.append(etag);
        builder.append(", created=");
        builder.append(created);
        builder.append(", changed=");
        builder.append(changed);
        builder.append(", specProjectWorkingGroupTid=");
        builder.append(specProjectWorkingGroupTid);
        builder.append("]");
        return builder.toString();
    }

    @Singleton
    public static class CommitterPaperworkFilter implements DtoFilter<CommitterPaperwork> {
        @Inject
        ParameterizedSQLStatementBuilder builder;
        @Inject
        ProfileService dus;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder
                    .buildWithId(TABLE,
                            Optional.ofNullable(params.getFirst(DefaultUrlParameterNames.ID_PARAMETER_NAME)),
                            Long.class);
            String forge = params.getFirst(CommitterPaperworkParameterNames.FORGE_NAME_RAW);
            if (StringUtils.isNotBlank(forge)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".forge = ?",
                                new Object[] { forge }));
            }
            String projectId = params.getFirst(CommitterPaperworkParameterNames.PROJECT_ID_NAME_RAW);
            if (StringUtils.isNotBlank(projectId)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".projectId = ?",
                                new Object[] { projectId }));
            }
            String uid = params.getFirst(CommitterPaperworkParameterNames.UID_NAME_RAW);
            String username = params.getFirst(CommitterPaperworkParameterNames.USERNAME_NAME_RAW);
            if (StringUtils.isNotBlank(uid)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".uid = ?",
                                new Object[] { uid }));
            } else if (StringUtils.isNotBlank(username)) {
                dus
                        .fetchUserByUsername(username, false)
                        .ifPresent(user -> statement
                                .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".uid = ?",
                                        new Object[] { user.uid() })));
            }

            String electionNid = params.getFirst(CommitterPaperworkParameterNames.ELECTION_NID_NAME_RAW);
            if (StringUtils.isNumeric(electionNid)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".electionNid = ?",
                                new Object[] { Integer.parseInt(electionNid) }));
            }
            String electionStatus = params.getFirst(CommitterPaperworkParameterNames.ELECTION_STATUS_NAME_RAW);
            if (StringUtils.isNumeric(electionStatus)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".electionStatus = ?",
                                new Object[] { Integer.parseInt(electionStatus) }));
            }

            String paperworkNid = params.getFirst(CommitterPaperworkParameterNames.COMMITTER_PAPERWORK_NID_NAME_RAW);
            if (StringUtils.isNumeric(paperworkNid)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".committerPaperworkNid = ?",
                                new Object[] { Integer.parseInt(paperworkNid) }));
            }
            String paperworkStatus = params
                    .getFirst(CommitterPaperworkParameterNames.COMMITTER_PAPERWORK_STATUS_NAME_RAW);
            if (StringUtils.isNumeric(paperworkStatus)) {
                statement
                        .addClause(
                                new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".committerPaperworkStatus = ?",
                                        new Object[] { Integer.parseInt(paperworkStatus) }));
            }

            List<String> excludedPaperworkStatus = params
                    .get(CommitterPaperworkParameterNames.COMMITTER_PAPERWORK_STATUS_EXCLUDED_NAME_RAW);
            if (excludedPaperworkStatus != null && !excludedPaperworkStatus.isEmpty()) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(
                                TABLE.getAlias() + ".committerPaperworkStatus NOT IN ?",
                                new Object[] { excludedPaperworkStatus
                                        .stream()
                                        .map(Integer::valueOf)
                                        .toList() }));
            }

            String specProjectWorkingGroup = params
                    .getFirst(CommitterPaperworkParameterNames.SPEC_PROJECT_WORKING_GROUP_NAME_RAW);
            if (StringUtils.isNumeric(specProjectWorkingGroup)) {
                statement
                        .addClause(
                                new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".specProjectWorkingGroup = ?",
                                        new Object[] { Integer.parseInt(specProjectWorkingGroup) }));
            }
            return statement;
        }

        @Override
        public Class<CommitterPaperwork> getType() {
            return CommitterPaperwork.class;
        }
    }
}
