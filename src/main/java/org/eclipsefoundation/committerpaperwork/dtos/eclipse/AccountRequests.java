/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.dtos.eclipse;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkParameterNames;
import org.eclipsefoundation.persistence.dto.BareNode;
import org.eclipsefoundation.persistence.dto.filter.DtoFilter;
import org.eclipsefoundation.persistence.model.DtoTable;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatement;
import org.eclipsefoundation.persistence.model.ParameterizedSQLStatementBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.core.MultivaluedMap;

/**
 * @author Martin Lowe
 *
 */
@Table(name = "account_requests")
@Entity
public class AccountRequests extends BareNode {
    public static final DtoTable TABLE = new DtoTable(AccountRequests.class, "ar");

    @EmbeddedId
    private AccountRequestsCompositeId id;
    private String newEmail;
    private String fname;
    private String lname;
    private String password;
    private String ip;
    private String token;

    /**
     * @return the id
     */
    @Override
    public AccountRequestsCompositeId getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(AccountRequestsCompositeId id) {
        this.id = id;
    }

    /**
     * @return the newEmail
     */
    public String getNewEmail() {
        return newEmail;
    }

    /**
     * @param newEmail the newEmail to set
     */
    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    /**
     * @return the fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * @param fname the fname to set
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * @return the lname
     */
    public String getLname() {
        return lname;
    }

    /**
     * @param lname the lname to set
     */
    public void setLname(String lname) {
        this.lname = lname;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Objects.hash(fname, id, ip, lname, newEmail, password, token);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AccountRequests other = (AccountRequests) obj;
        return Objects.equals(fname, other.fname) && Objects.equals(id, other.id) && Objects.equals(ip, other.ip)
                && Objects.equals(lname, other.lname) && Objects.equals(newEmail, other.newEmail)
                && Objects.equals(password, other.password) && Objects.equals(token, other.token);
    }

    @Embeddable
    public static class AccountRequestsCompositeId implements Serializable {
        private static final long serialVersionUID = -6301337509368582680L;

        private String email;
        @Column(name = "req_when")
        private ZonedDateTime reqWhen;

        /**
         * @return the email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email the email to set
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return the reqWhen
         */
        public ZonedDateTime getReqWhen() {
            return reqWhen;
        }

        /**
         * @param reqWhen the reqWhen to set
         */
        public void setReqWhen(ZonedDateTime reqWhen) {
            this.reqWhen = reqWhen;
        }

        @Override
        public int hashCode() {
            return Objects.hash(email, reqWhen);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            AccountRequestsCompositeId other = (AccountRequestsCompositeId) obj;
            return Objects.equals(email, other.email) && Objects.equals(reqWhen, other.reqWhen);
        }

    }

    @Singleton
    public static class AccountRequestsFilter implements DtoFilter<AccountRequests> {
        private static final Logger LOGGER = LoggerFactory.getLogger(AccountRequestsFilter.class);

        @Inject
        ParameterizedSQLStatementBuilder builder;

        @Override
        public ParameterizedSQLStatement getFilters(MultivaluedMap<String, String> params, boolean isRoot) {
            ParameterizedSQLStatement statement = builder.build(TABLE);

            // email column maps to username in our cases, so we should lookup using that value
            String username = params.getFirst(CommitterPaperworkParameterNames.USERNAME_NAME_RAW);
            if (StringUtils.isNotBlank(username)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id.email = ?",
                                new Object[] { username }));
            }
            // do a safe lookup of the request time
            String reqDate = params.getFirst(CommitterPaperworkParameterNames.REQ_WHEN_NAME_RAW);
            try {
                if (StringUtils.isNotBlank(reqDate)) {
                    statement
                            .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".id.reqWhen = ?",
                                    new Object[] { LocalDateTime.parse(reqDate) }));
                }
            } catch (DateTimeParseException e) {
                LOGGER.warn("Bad date string passed for date lookup", e);
                throw new BadRequestException();
            }

            String token = params.getFirst(CommitterPaperworkParameterNames.TOKEN_NAME_RAW);
            if (StringUtils.isNotBlank(token)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".token = ?",
                                new Object[] { token }));
            }

            // check the operation through first and last name
            String op = params.getFirst(CommitterPaperworkParameterNames.PERMISSION_OPERATION_NAME_RAW);
            if (StringUtils.isNotBlank(op)) {
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".fname = ?",
                                new Object[] { op }));
                statement
                        .addClause(new ParameterizedSQLStatement.Clause(TABLE.getAlias() + ".lname = ?",
                                new Object[] { op }));
            }
            return statement;
        }

        @Override
        public Class<AccountRequests> getType() {
            return AccountRequests.class;
        }
    }
}
