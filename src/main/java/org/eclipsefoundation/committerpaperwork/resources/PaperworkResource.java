/**
 * Copyright (c) 2023 Eclipse Foundation
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipsefoundation.committerpaperwork.resources;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.committerpaperwork.dtos.eclipseapi.CommitterPaperwork;
import org.eclipsefoundation.committerpaperwork.models.PaperworkData;
import org.eclipsefoundation.committerpaperwork.models.PaperworkListData;
import org.eclipsefoundation.committerpaperwork.models.PaperworkResultData;
import org.eclipsefoundation.committerpaperwork.models.PaperworkUpdateData;
import org.eclipsefoundation.committerpaperwork.models.ProspectivePaperworkData;
import org.eclipsefoundation.committerpaperwork.models.ProvisioningRequestData;
import org.eclipsefoundation.committerpaperwork.models.ResultURLData;
import org.eclipsefoundation.committerpaperwork.models.RetirementRequestData;
import org.eclipsefoundation.committerpaperwork.models.mappers.CommitterPaperworkMapper;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkAuthScopes;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkElectionStatus;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkParameterNames;
import org.eclipsefoundation.committerpaperwork.namespaces.CommitterPaperworkStatus;
import org.eclipsefoundation.committerpaperwork.services.PaperworkService;
import org.eclipsefoundation.committerpaperwork.services.ProjectsService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.services.ProfileService;
import org.eclipsefoundation.foundationdb.client.runtime.model.project.ProjectData;
import org.eclipsefoundation.http.model.RequestWrapper;
import org.eclipsefoundation.http.namespace.DefaultUrlParameterNames;
import org.eclipsefoundation.persistence.dao.impl.DefaultHibernateDao;
import org.eclipsefoundation.persistence.model.RDBMSQuery;
import org.eclipsefoundation.persistence.service.FilterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.security.Authenticated;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.persistence.NoResultException;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;

/**
 * Endpoint for requests for and to update committer paperwork data.
 * 
 * @author Martin Lowe
 *
 */
@Path("")
@Authenticated
public class PaperworkResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(PaperworkResource.class);

    public static final List<String> VALID_RETIREMENT_ROLES = Collections.unmodifiableList(Arrays.asList("CM", "PL"));
    private static final String USER_NOT_FOUND_MESSAGE_FORMAT = "Cannot find a valid Eclipse Foundation user with name '%s'";
    private static final int DEFAULT_PAPERWORK_STATUS = 9999;

    @Inject
    DefaultHibernateDao dao;
    @Inject
    CommitterPaperworkMapper mapper;
    @Inject
    FilterService filters;

    @Inject
    ProfileService dus;
    @Inject
    ProjectsService projects;
    @Inject
    PaperworkService paperworkService;

    @Inject
    RequestWrapper wrap;
    @Context
    UriInfo uriInfo;

    @GET
    @RolesAllowed(CommitterPaperworkAuthScopes.READ)
    public Response getPaperwork() {
        return Response
                .ok(Arrays
                        .asList(PaperworkListData
                                .builder()
                                .setResult(dao
                                        .get(new RDBMSQuery<>(wrap, filters.get(CommitterPaperwork.class)))
                                        .parallelStream()
                                        .map(mapper::toModel)
                                        .toList())
                                .build()))
                .build();
    }

    @POST
    @Path("{username}")
    @RolesAllowed(CommitterPaperworkAuthScopes.WRITE)
    public Response createPaperwork(@PathParam("username") String username,
            ProspectivePaperworkData prospectivePaperwork) {
        // validate the user and get the actual user object (cached)
        Optional<EfUser> efUser = dus.fetchUserByUsername(username, true);
        if (efUser.isEmpty()) {
            throw new NotFoundException(String.format(USER_NOT_FOUND_MESSAGE_FORMAT, username));
        }

        // validate the election status code
        if (CommitterPaperworkElectionStatus
                .getStatusByCode(prospectivePaperwork.getParameters().getElectionStatus())
                .isEmpty()) {
            throw new BadRequestException("Unexpected election status passed, cannot continue to process: "
                    + prospectivePaperwork.getParameters().getElectionStatus());
        }

        // check that there isn't inprogress paperwork for params
        checkForExistingPaperwork(username, prospectivePaperwork);

        // return the new paperwork once created
        return Response
                .status(Status.CREATED)
                .entity(ResultURLData
                        .builder()
                        .setUrl(uriInfo.getAbsolutePath().toString() + "/"
                                + generateNewPaperwork(prospectivePaperwork, efUser.get()).getId())
                        .build())
                .build();
    }

    @GET
    @Path("{username}/{id}")
    @RolesAllowed(CommitterPaperworkAuthScopes.READ)
    public PaperworkData getPaperworkForUser(@PathParam("username") String username, @PathParam("id") Integer id) {
        // validate the user and get the actual user object (cached)
        Optional<EfUser> efUser = dus.fetchUserByUsername(username, true);
        if (efUser.isEmpty()) {
            throw new NotFoundException(String.format(USER_NOT_FOUND_MESSAGE_FORMAT, username));
        }

        // additional params to include in request
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), Integer.toString(id));
        params.add(CommitterPaperworkParameterNames.USERNAME_NAME_RAW, efUser.get().name());
        // retrieve data and then build response
        List<CommitterPaperwork> results = dao
                .get(new RDBMSQuery<>(wrap, filters.get(CommitterPaperwork.class), params));
        if (results == null || results.isEmpty()) {
            throw new NoResultException("No matching paperwork for given result");
        }
        return mapper.toModel(results.get(0));

    }

    @POST
    @Path("{username}/{id}")
    @RolesAllowed(CommitterPaperworkAuthScopes.WRITE)
    public ResultURLData updatePaperworkForUser(@PathParam("username") String username, @PathParam("id") Integer id,
            PaperworkUpdateData updateRequest) {
        // validate the user and get the actual user object (cached)
        Optional<EfUser> efUser = dus.fetchUserByUsername(username, true);
        if (efUser.isEmpty()) {
            throw new NotFoundException(String.format(USER_NOT_FOUND_MESSAGE_FORMAT, username));
        }

        // validate the paperwork status code
        if (CommitterPaperworkStatus
                .getStatusByCode(updateRequest.getParameters().getCommitterPaperworkStatus())
                .isEmpty()) {
            throw new BadRequestException("Unexpected paperwork status passed, cannot continue to process: "
                    + updateRequest.getParameters().getCommitterPaperworkStatus());
        }

        // additional params to include in request
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), Integer.toString(id));
        params.add(CommitterPaperworkParameterNames.USERNAME_NAME_RAW, efUser.get().name());
        // retrieve data and check the results for paperwork to update
        List<CommitterPaperwork> results = dao
                .get(new RDBMSQuery<>(wrap, filters.get(CommitterPaperwork.class), params));
        if (results == null || results.isEmpty()) {
            throw new NoResultException(String
                    .format("No committer paperwork found for user '%s' and paperwork ID '%s'", username,
                            Integer.toString(id)));
        }

        // update the paperwork
        CommitterPaperwork updateTarget = results.get(0);
        updateTarget.setCommitterPaperworkNid(updateRequest.getParameters().getCommitterPaperworkNid());
        updateTarget.setCommitterPaperworkStatus(updateRequest.getParameters().getCommitterPaperworkStatus());
        updateTarget.setChanged(TimeUnit.SECONDS.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS));
        List<CommitterPaperwork> updatedDocs = dao
                .add(new RDBMSQuery<>(wrap, filters.get(CommitterPaperwork.class)), Arrays.asList(updateTarget));
        if (updatedDocs == null || updatedDocs.isEmpty()) {
            throw new ServerErrorException("Could not update committer paperwork documents",
                    Response.Status.INTERNAL_SERVER_ERROR);
        }

        return ResultURLData.builder().setUrl(uriInfo.getAbsolutePath().toString()).build();
    }

    @DELETE
    @Path("{username}/{id}")
    @RolesAllowed(CommitterPaperworkAuthScopes.DELETE)
    public Response deletePaperworkForUser(@PathParam("username") String username, @PathParam("id") Integer id) {
        // validate the user and get the actual user object (cached)
        Optional<EfUser> efUser = dus.fetchUserByUsername(username, true);
        if (efUser.isEmpty()) {
            throw new NotFoundException(String.format(USER_NOT_FOUND_MESSAGE_FORMAT, username));
        }

        // additional params to include in request
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(DefaultUrlParameterNames.ID.getName(), Integer.toString(id));
        params.add(CommitterPaperworkParameterNames.USERNAME_NAME_RAW, efUser.get().name());

        dao.delete(new RDBMSQuery<>(wrap, filters.get(CommitterPaperwork.class), params));
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    /**
     * Retire a committer from a given project and role.
     * 
     * @param username The Eclipse Foundation username of the user to retire
     * @param request body of HTTP request containing the parameters for the retirement request
     * @return an OK response if the retirement is successful
     * @throws BadRequestException if there are validation errors while preparing the retirement
     */
    @POST
    @Path("{username}/retire")
    @RolesAllowed(CommitterPaperworkAuthScopes.DELETE)
    public Response retireUserPaperwork(@PathParam("username") String username, RetirementRequestData request) {
        // check the project role
        if (StringUtils.isBlank(request.getInfo().getProjectRole())
                || !VALID_RETIREMENT_ROLES.contains(request.getInfo().getProjectRole())) {
            throw new BadRequestException("The supplied project role can't be retired.");
        }
        Optional<EfUser> requestorUser = dus.fetchUserByUsername(request.getInfo().getRequestor(), true);
        if (requestorUser.isEmpty()) {
            throw new NotFoundException(String.format(USER_NOT_FOUND_MESSAGE_FORMAT, request.getInfo().getRequestor()));
        }

        // validate project ID
        Optional<ProjectData> project = projects.getProject(request.getInfo().getProjectId());
        if (project.isEmpty()) {
            throw new BadRequestException("Project ID does not validate.");
        }

        // start workflow to retire the given user
        paperworkService.retireUser(request, username, requestorUser.get());

        return Response.ok().build();
    }

    /**
     * Starts the user provisioning process for a given project and user for the committer role.
     * 
     * @param username the EF username that is being provisioned
     * @param id the paperwork ID to use in provisioning
     * @param request additional request information
     * @return an OK response for successful provisioning, otherwise an error response.
     */
    @POST
    @Path("{username}/provisioning/{id}")
    @RolesAllowed(CommitterPaperworkAuthScopes.DELETE)
    public Response provisionCommitterPaperwork(@PathParam("username") String username, @PathParam("id") String id,
            ProvisioningRequestData request) {
        // validate the paperwork ID
        if (!StringUtils.isNumeric(id)) {
            throw new BadRequestException("Committer paperwork ID must be numeric: " + id);
        }
        // validate the user and get the actual user object (cached)
        Optional<EfUser> user = dus.fetchUserByUsername(username, true);
        if (user.isEmpty()) {
            throw new NotFoundException(String.format(USER_NOT_FOUND_MESSAGE_FORMAT, username));
        }

        // get the actual paperwork for the request
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add("id", id);
        List<CommitterPaperwork> paperworkResults = dao
                .get(new RDBMSQuery<CommitterPaperwork>(wrap, filters.get(CommitterPaperwork.class), params));
        if (paperworkResults.isEmpty()) {
            throw new BadRequestException("Could not find paperwork with ID " + id);
        }
        CommitterPaperwork paperwork = paperworkResults.get(0);

        // validate the status of the paperwork and election status before processing
        if (paperwork.getCommitterPaperworkStatus() != CommitterPaperworkStatus.COMPLETED.getCode()) {
            throw new BadRequestException("The status of the committer paperwork is not complete");
        } else if (paperwork.getElectionStatus() != CommitterPaperworkElectionStatus.ELECTION_APPROVED.getCode()) {
            throw new BadRequestException("The election status of the committer paperwork is not approved");
        }

        // start workflow to provision the user for the given paperwork
        String provisioningValue = paperworkService.provisionUser(request, paperwork, user.get());
        return Response.ok(PaperworkResultData.builder().setBody(provisioningValue).setResult(true).build()).build();
    }

    /**
     * Generate and return a new persisted CommitterPaperwork object for the current request. This call assumes that
     * checks for uniqueness to avoid collision have already been completed.
     * 
     * @param prospectivePaperwork the request payload with information for the new CommitterPaperwork instance
     * @param efUser the user that will own the new paperwork instance
     * @return the newly generated paperwork entity
     */
    private CommitterPaperwork generateNewPaperwork(ProspectivePaperworkData prospectivePaperwork, EfUser efUser) {
        // map the prospective paperwork to the new paperwork data
        CommitterPaperwork paperwork = new CommitterPaperwork();
        paperwork.setForge(prospectivePaperwork.getParameters().getForge());
        paperwork.setProjectId(prospectivePaperwork.getParameters().getProjectId());
        paperwork.setElectionNid(prospectivePaperwork.getParameters().getElectionNid());
        paperwork.setElectionStatus(prospectivePaperwork.getParameters().getElectionStatus());
        paperwork.setCommitterPaperworkStatus(DEFAULT_PAPERWORK_STATUS);
        paperwork.setCreated(TimeUnit.SECONDS.convert(Duration.of(System.currentTimeMillis(), ChronoUnit.MILLIS)));
        paperwork.setChanged(TimeUnit.SECONDS.convert(Duration.of(System.currentTimeMillis(), ChronoUnit.MILLIS)));
        paperwork.setUid(efUser.uid());
        paperwork.setEtag(paperwork.generateEtag());

        // perform the update and then return if successful
        List<CommitterPaperwork> results = dao
                .add(new RDBMSQuery<>(wrap, filters.get(CommitterPaperwork.class)), Arrays.asList(paperwork));
        if (results == null || results.isEmpty()) {
            LOGGER
                    .warn("Unable to generate paperwork for user '{}' in project '{}'", efUser.name(),
                            prospectivePaperwork.getParameters().getProjectId());
            throw new ServerErrorException(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return results.get(0);
    }

    /**
     * Check to see whether the user has other active paperwork in progress for the given project/request before
     * continuing to process.
     * 
     * @param username the user to check for active paperwork entities
     * @param prospectivePaperwork the current request to generate new paperwork
     */
    private void checkForExistingPaperwork(String username, ProspectivePaperworkData prospectivePaperwork) {
        // prepare query to check for existing paperwork
        MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
        params.add(CommitterPaperworkParameterNames.USERNAME_NAME_RAW, username);
        params
                .add(CommitterPaperworkParameterNames.PROJECT_ID_NAME_RAW,
                        prospectivePaperwork.getParameters().getProjectId());
        params
                .add(CommitterPaperworkParameterNames.ELECTION_NID_NAME_RAW,
                        Integer.toString(prospectivePaperwork.getParameters().getElectionNid()));
        // add param to exclude finished paperwork workflow entities from search
        params
                .addAll(CommitterPaperworkParameterNames.COMMITTER_PAPERWORK_STATUS_EXCLUDED_NAME_RAW,
                        Arrays
                                .asList(CommitterPaperworkStatus.PROVISIONED, CommitterPaperworkStatus.COMPLETED,
                                        CommitterPaperworkStatus.FAILED)
                                .stream()
                                .map(status -> Integer.toString(status.getCode()))
                                .toList());
        // get paperwork for the current user to check for duplicates
        List<CommitterPaperwork> existingPaperwork = dao
                .get(new RDBMSQuery<>(wrap, filters.get(CommitterPaperwork.class), params));
        if (!existingPaperwork.isEmpty()) {
            throw new WebApplicationException("User already has active paperwork for given project and election",
                    Response.Status.CONFLICT);
        }
    }
}
